function [seg] = generate_straight_segment(v_in, v_out, s_ground, X0, heading_g, wind_speed)
    % get conservative air-frame distance
    [s_air, ~, ~] = get_conservative_airframe_sf(...
            v_in, v_out, wind_speed, s_ground, heading_g);
    
    % get acceleration
    a_air = (v_out^2 - v_in^2) / (2*s_air);
    
    % construct straight segment
    seg.straight = true;
    seg.a_air = a_air;
    seg.v_in = v_in;
    seg.v_out = v_out;
    seg.heading_g = heading_g;
    seg.X0 = X0;
    seg.heading_a1 = get_heading_from_deriv(heading_g, wind_speed, v_in);
    seg.heading_a2 = get_heading_from_deriv(heading_g, wind_speed, v_out);
    seg.s_air = s_air;
    seg.s_ground = s_ground;
end