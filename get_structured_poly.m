function [turn, flag] = get_structured_poly(model, consts, allowed_err, waypoints, heading1, heading2, wind_speed)

    %% Get initial and final curvatures headings (in the air-frame) for given ground-path headings
    curv_heading1 = get_heading_from_deriv(heading1, wind_speed, model.max_airspeed);
    curv_heading2 = get_heading_from_deriv(heading2, wind_speed, model.max_airspeed);
    
    %% Check for the degenerate case
    if abs(get_corr_heading_change(heading1, heading2)) < 1e-4
        turn.Sf1 = 0;
        turn.Sf2 = 0;
        turn.Sf3 = 0;
        turn.k_trans = 0;
        turn.X0 = [0 0];
        turn.curv_heading1 = curv_heading1;
        turn.curv_heading2 = curv_heading2;
        turn.constraints1 = [];
        turn.constraints3 = [];
        turn.empty = true;
        flag = true;
        return
    end
    
    %% Get turn limits
%     [model] = get_turn_limits(waypoints, model);

    %% Initialize
    if get_corr_heading_change(heading1, heading2) > 0
        k_trans = model.curv_max*0.9;
        lb_curv = model.curv_max/20;
        ub_curv = model.curv_max;
    else
        k_trans = -model.curv_max*0.9;
        lb_curv = -model.curv_max;
        ub_curv = -model.curv_max/20;
    end
    
    constraints1.bv = [0; k_trans];
    constraints1.bv_deriv = [0; 0];
    constraints1.curv_rate = model.curv_rate_max;
    constraints1.curv_max = model.curv_max;
    constraints1.curv_rate_rate = model.curv_rate_max;
    constraints3.bv = [k_trans; 0];
    constraints3.bv_deriv = [0; 0];
    constraints3.curv_rate = model.curv_rate_max;
    constraints3.curv_max = model.curv_max;
    constraints3.curv_rate_rate = model.curv_rate_max;

    [C1, Sf1, ~] = init_curv_poly_new(consts.CURV_POLY_DEG, constraints1);
    [C3, Sf3, ~] = init_curv_poly_new(consts.CURV_POLY_DEG, constraints3);
    [Sf2, ~] = get_initial_sf2(Sf1, C1, Sf3, C3, k_trans, curv_heading1, curv_heading2);
%     Sf2 = eps;
    
    %% Set up optimizer
    X0 = [Sf1; Sf2; Sf3; k_trans];
    %% TODO - make this heading-change dependent!!!!!!!
    lb = [eps; 0; eps; lb_curv];
    ub = [Sf1*10; 1000; Sf3*10; ub_curv];
    
    cost_func = @(x) cost_fn_structured(x, wind_speed, waypoints, curv_heading1, heading2, consts, model, constraints1, constraints3);
    constr_func = @(x) constraint_fn_structured(x, wind_speed, waypoints, curv_heading1, heading2, consts, model, constraints1, constraints3, allowed_err);
    options = optimset('MaxFunEvals', 1000000, 'Display', 'iter', 'MaxIter', 100);

    [X_opt, fval, exitflag, output] = fmincon(cost_func, X0, [], [], [], [], ...
        lb, ub, constr_func, options);
    
    Sf1 = X_opt(1); Sf3 = X_opt(3); Sf2 = X_opt(2); k_trans = X_opt(4);
    
    %% Display path
    disp_route_structured(X_opt, wind_speed, waypoints, curv_heading1, heading2, consts, model, constraints1, constraints3);
 
    %% Check for constraint violations
    [violations, flag] = curv_constraints_checker_structured(X_opt, wind_speed, waypoints, curv_heading1, ...
        heading2, consts, model, allowed_err, constraints1, constraints3);
    
    turn.Sf1 = Sf1;
    turn.Sf2 = Sf2;
    turn.Sf3 = Sf3;
    turn.k_trans = k_trans;
    turn.X0 = [0 0];
    turn.curv_heading1 = curv_heading1;
    turn.curv_heading2 = curv_heading2;
    turn.constraints1 = constraints1;
    turn.constraints3 = constraints3;
    turn.empty = false;
    turn.limits = model;
end