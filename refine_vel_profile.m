function [route] = refine_vel_profile(route, vels, seg_s, seg_v, seg_a, seg_j, seg_maxV, bv, consts)
    tol = 1.5;

    %% Compress list of velocities...
    % ...by locating monotonic sequences
    new_segments = cell(length(seg_s), 1);
    vels = [vels; bv.end(1)];
    
    % start a new segment
    num_new_segs = 0; 
    seg.init_vel = bv.init(1);
    seg.start_idx = 1;
    diff = vels(1) - bv.init(1);
    if abs(diff) < tol
        diff = 0;
    end
    sgn = sign(diff);
    if sgn ~= 0 
        seg.straight = false; 
    else
        seg.straight = true;
    end
    min_seg_max = seg_maxV(1);
    
    for i = 2:length(seg_s)
         diff = vels(i) - vels(i-1);
         if abs(diff) < tol
            diff = 0;
         end
         curr_sgn = sign(diff);
         if (curr_sgn == sgn) && (seg_maxV(i) <= min_seg_max)
             % same segment
             if seg_maxV(i) < min_seg_max
                 min_seg_max = seg_maxV(i);
             end
             continue;
         else
             % end old segment, and start a new one
             if seg.straight
                 vels(i-1) = seg.init_vel;
             end
             seg.final_vel = vels(i-1);
             seg.s = sum(seg_s(seg.start_idx:i-1));
%              seg.vlim = max(seg.init_vel, seg.final_vel); % TODO -> think about this for varying seg vel limits
             seg.vlim = max(seg_maxV(seg.start_idx:i-1)); % TODO -> think about this for varying seg vel limits
             seg.alim = max(seg_a(seg.start_idx:i-1));
             seg.jlim = max(seg_j(seg.start_idx:i-1));
             seg.end_idx = i-1;
             num_new_segs = num_new_segs + 1;
             new_segments{num_new_segs} = seg;
             
             seg.init_vel = vels(i-1);
             seg.start_idx = i;
             sgn = curr_sgn;
             if sgn == 0
                seg.straight = true;
             else
                seg.straight = false;
             end
         end        
    end
    
    % End last segment
    seg.final_vel = vels(end);
    seg.s = sum(seg_s(seg.start_idx:end));
    seg.vlim = max(seg.init_vel, seg.final_vel);
    seg.alim = seg_a(end);
    seg.jlim = seg_j(end);
    seg.end_idx = length(seg_s);
    seg.straight = false;
    num_new_segs = num_new_segs + 1;
    new_segments{num_new_segs} = seg;
    
    %% Fit smooth polynomials to the segments
    vel_polys = cell(num_new_segs, 1);
    prev_s = 0;
    for i = 1:num_new_segs
        seg = new_segments{i};
%         constraints.bv = [seg.init_vel; seg.final_vel];
%         constraints.bv_deriv = [0; 0];
%         constraints.bv_dderiv = [];
%         constraints.bv_integral = seg.s;
%         constraints.speed_max = seg.vlim+0.5;
%         constraints.accel = seg.alim;
%         constraints.jerk = seg.jlim;
%         constraints.straight = seg.straight;
%         
%         [vel_polys{i}] = init_vel_poly(consts.SPEED_POLY_DEG, constraints, constraints.speed_max, 10);
        limits.max_accel = seg.alim; limits.max_jerk = seg.jlim;
        [vel_polys{i}] = get_naive_accel_profile(seg.init_vel, seg.final_vel, seg.s, limits, consts, prev_s);
        prev_s = prev_s + vel_polys{i}.Sf_total;
    end
    
    %% Collect all vel polys into a piecewise poly structure
    breaks = [0]; coeffs_vel = []; coeffs_s = [];
    t_offset = 0;
    for i = 1:length(vel_polys)
        vel = vel_polys{i};
        if vel.empty
            breaks = [breaks; t_offset+vel.tf4];
            coeffs_vel = [coeffs_vel; vel.vel_C4];
            coeffs_s = [coeffs_s; vel.Sf_C4];
            t_offset = t_offset + vel.tf4;
            continue;
        end
        
        % TODO -> make this cleaner (collect in array and iterate?)
        if vel.tf1 > eps
            breaks = [breaks; t_offset+vel.tf1];
            coeffs_vel = [coeffs_vel; vel.vel_C1];
            coeffs_s = [coeffs_s; vel.Sf_C1];
            t_offset = t_offset + vel.tf1;
        end
        if vel.tf2 > eps
            breaks = [breaks; t_offset+vel.tf2];
            coeffs_vel = [coeffs_vel; vel.vel_C2];
            coeffs_s = [coeffs_s; vel.Sf_C2];
            t_offset = t_offset + vel.tf2;
        end
        if vel.tf3 > eps
            breaks = [breaks; t_offset+vel.tf3];
            coeffs_vel = [coeffs_vel; vel.vel_C3];
            coeffs_s = [coeffs_s; vel.Sf_C3];
            t_offset = t_offset + vel.tf3;
        end
        if vel.tf4 > eps
            breaks = [breaks; t_offset+vel.tf4];
            coeffs_vel = [coeffs_vel; vel.vel_C4];
            coeffs_s = [coeffs_s; vel.Sf_C4];
            t_offset = t_offset + vel.tf4;
        end
    end
    vel_prof = mkpp(breaks, coeffs_vel);
    s_prof = mkpp(breaks, coeffs_s);
    
    route.vel_prof = vel_prof;
    route.s_prof = s_prof;
    route.vel_polys = vel_polys;
    route.total_t = t_offset;
end