function [turn, flag] = fit_poly_to_wayline(waypts, model, consts, allowed_err, wind_speed)
    %% constants
    dist_tol = 1e-4;
    
    %% parse waypoints
    angles = normr(diff(waypts(:, 1:2)));
    angles = atan2(angles(:, 2), angles(:, 1));
    angles = wrapTo2Pi(angles);
    heading1 = angles(1);
    heading2 = angles(2);
    
    %% Get tunnels
    tunnel1 = generate_corridor(waypts(1, 1:2), waypts(2, 1:2), waypts(1, 5), waypts(1, 6));
    tunnel2 = generate_corridor(waypts(2, 1:2), waypts(3, 1:2), waypts(2, 5), waypts(2, 6));
    
    %% Get turn limits
    [model] = get_turn_limits(waypts, model);
    
    %% Iterate over speeds until we find a feasible turn
    step = consts.curvs.vel_step;
    low_speed = consts.curvs.min_vel;
    model.max_airspeed = model.max_airspeed;
    X0 = waypts(1, 1:2) + 0.5 * (waypts(2, 1:2) - waypts(1, 1:2));
    Xlim = waypts(2, 1:2) + 0.5 * (waypts(3, 1:2) - waypts(2, 1:2));
    path_X = []; path_Y = [];
    valid_turn = false;
    turn = [];
    while model.max_airspeed >= low_speed
        [model] = get_turn_limits(waypts, model, true);
        
        % iterate to see if we can fit a turn at this airspeed
        count = 1;
        while count <= consts.curvs.num_curv / 2
            [turn, flag, count] = get_naive_curv_turn(model, consts, allowed_err, heading1, heading2, wind_speed, count);
             count = count + 1;
            if ~flag continue; end
            if turn.empty
                turn.X0 = [waypts(2, 1) waypts(2, 2)];
                turn.end = turn.X0; 
                return;
            end

            % Check if the end-point is within 1/2 segment length limits
            % Get path
    %         [path_X, path_Y, ~, ~, ~, ~, ~, ~, ~] = ...
    %             get_path_structured([turn.Sf1 turn.Sf2 turn.Sf3 turn.k_trans], wind_speed, waypts, turn.curv_heading1, heading2, ...
    %                 consts, model, turn.constraints1, turn.constraints3);
    %         X_end = [path_X(end) path_Y(end)];
            X_end = get_turn_end_point(turn, turn.curv_heading1, wind_speed);
            [X_init, X_end, matched] = find_curv_poly_fit(X_end, [X0; waypts(2, 1:2); Xlim], dist_tol);
            if matched
                turn.X0 = X_init;
                turn.end = X_end;
                %% Set up piecewise polynomial
                breaks = [0]; curv_coeffs = []; psi_coeffs = [];
                prev_psi = turn.curv_heading1;
                prev_S = 0;
                if turn.Sf1 > eps
                    breaks = [breaks; turn.Sf1];
                    curv_coeffs = [curv_coeffs; turn.C1];
                    psi_coeffs = [psi_coeffs; polyint(turn.C1, prev_psi)];
                    prev_psi = polyval(psi_coeffs(end, :), turn.Sf1);
                    prev_S = prev_S + turn.Sf1;
                end
                if turn.Sf2 > eps
                    breaks = [breaks; prev_S+turn.Sf2];
                    curv_coeffs = [curv_coeffs; turn.C2];
                    psi_coeffs = [psi_coeffs; polyint(turn.C2, prev_psi)];
                    prev_psi = polyval(psi_coeffs(end, :), turn.Sf2);
                    prev_S = prev_S + turn.Sf2;
                end
                if turn.Sf3 > eps
                    breaks = [breaks; prev_S+turn.Sf3];
                    curv_coeffs = [curv_coeffs; turn.C3];
                    psi_coeffs = [psi_coeffs; polyint(turn.C3, prev_psi)];
                end
                turn.curv_pp = mkpp(breaks, curv_coeffs);
                turn.heading_pp = mkpp(breaks, psi_coeffs);

                %% check for corridor
                [X, Y, ~, ~, ~, ~, ~, ~, ~] = get_path_from_piecewise_poly(turn, wind_speed, ...
                    linspace(0, turn.Sf_total, 100), consts);
                valid_turn = tunnel_check([X' Y'], tunnel1, tunnel2);
                if valid_turn
                    flag = true;
                    return;
                end
            end
        end
        
        model.max_airspeed = model.max_airspeed - step;
    end
    
    if ~valid_turn
        flag = false;
        return;
    end
    
%     %% Set up piecewise polynomial
%     breaks = [0]; curv_coeffs = []; psi_coeffs = [];
%     prev_psi = turn.curv_heading1;
%     prev_S = 0;
%     if turn.Sf1 > eps
%         breaks = [breaks; turn.Sf1];
%         curv_coeffs = [curv_coeffs; turn.C1];
%         psi_coeffs = [psi_coeffs; polyint(turn.C1, prev_psi)];
%         prev_psi = polyval(psi_coeffs(end, :), turn.Sf1);
%         prev_S = prev_S + turn.Sf1;
%     end
%     if turn.Sf2 > eps
%         breaks = [breaks; prev_S+turn.Sf2];
%         curv_coeffs = [curv_coeffs; turn.C2];
%         psi_coeffs = [psi_coeffs; polyint(turn.C2, prev_psi)];
%         prev_psi = polyval(psi_coeffs(end, :), turn.Sf2);
%         prev_S = prev_S + turn.Sf2;
%     end
%     if turn.Sf3 > eps
%         breaks = [breaks; prev_S+turn.Sf3];
%         curv_coeffs = [curv_coeffs; turn.C3];
%         psi_coeffs = [psi_coeffs; polyint(turn.C3, prev_psi)];
%     end
%     turn.curv_pp = mkpp(breaks, curv_coeffs);
%     turn.heading_pp = mkpp(breaks, psi_coeffs);
    
%     %% Get structured turn
% %     [turn, flag] = get_structured_poly(model, consts, allowed_err, waypts, heading1, heading2, wind_speed);
%     [turn, flag] = get_naive_curv_turn(model, consts, allowed_err, waypts, heading1, heading2, wind_speed);
%     % Check for empty turns
%     if turn.empty
%         turn.X0 = [waypts(2, 1) waypts(2, 2)];
%         turn.end = turn.X0;
%         return;
%     end
%     
%     
%     %% Get path
%     [path_X, path_Y, ~, ~, ~, ~, ~, ~, ~] = ...
%         get_path_structured([turn.Sf1 turn.Sf2 turn.Sf3 turn.k_trans], wind_speed, waypts, turn.curv_heading1, heading2, ...
%             consts, model, turn.constraints1, turn.constraints3);
    
%     %% Shift turn to match up with waypoints
%     matched = false;
%     low = 0;
%     high = 1;
%     t = 0.5;
%     while t < 1
%         t = (low + high) / 2;
%         X0 = waypts(1, 1:2) + t * (waypts(2, 1:2) - waypts(1, 1:2));
%         X_end = X0 + [path_X(end) path_Y(end)];
%         
%         % check if we've converged
%         dist = point_dist_from_line(X_end, waypts(2, 1:2), waypts(3, 1:2));
%         if dist < dist_tol
%             matched = true;
%             break;
%         end
%         
%         % Shift start point otherwise
%         if point_in_triangle(X_end', [waypts(:, 1) waypts(:, 2)]')
%             low = t;
%         else
%             high = t;
%         end
%     end
%     
%     if matched
%         turn.X0 = waypts(1, 1:2) + t * (waypts(2, 1:2) - waypts(1, 1:2));
%         turn.end = turn.X0 + [path_X(end) path_Y(end)];
%     end
end