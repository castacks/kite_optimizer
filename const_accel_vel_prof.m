function [tf, feasible] = const_accel_vel_prof(vin, vout, S, a, vmax)
    % Check for feasibility
    feasible = abs(vout^2 - vin^2) * 0.5 / a - S;
    
   if abs(feasible)  < 1e-4
        tf = abs(vout - vin) / a;
        feasible = 0;
        return;
    elseif feasible > 0
%         tf = 1e4;
        tf = abs(vout - vin) / a;
        return;
    end
    
    S1 = (vmax^2 - vin^2) * 0.5 / a; S2 = (vmax^2 - vout^2) * 0.5 / a;
    if S1 + S2 < S
        tf = (vmax-vin + vmax-vout)/a + (S-S1-S2)/vmax;
    else
        vmid = sqrt((2*a*S + vin*vin + vout*vout)/2);
        tf = (vmid - vin + vmid - vout) / a;
    end
end