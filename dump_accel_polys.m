amax = 0.1 * 9.81;
amin = 0.1 * amax;
num_steps = 20;
jmax = 0.1 * 9.81;
degree = 3;

avals = linspace(-amax, amax, num_steps);

%% Setup constraints
constraints.bv_deriv = [0; 0];
constraints.jerk = jmax;
constraints.accel = amax;

%% Get polys
data = [];
for a = avals
    % get c1
    constraints.bv = [0; a];
    [C1, tf1, flag1] = init_accel_poly(degree, constraints);
    constraints.bv = [a; 0];
    [C2, tf2, flag2] = init_accel_poly(degree, constraints);
    
    if flag1 && flag2
        data = [data; [a, tf1, tf2, C1, C2]];
    end
end

csvwrite('lut/accel_polys.txt', data);