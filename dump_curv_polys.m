g = 9.81;
v_step = 2;
v_vals = 20:v_step:50;
max_roll = deg2rad(15);
max_roll_rate = deg2rad(15);
max_roll_rate_rate = deg2rad(30);
max_accel = 0.05 * g;

data = [];
num_fail = 0;
for v = v_vals
    curv_max = g * tan(max_roll) / (v^2);
    curv_rate_max = g * max_roll_rate / (v^3) - 2 * curv_max * max_accel / (v^2);
    curv_rate_rate_max = g * max_roll_rate_rate / v^4;
    
    curv_vals = [curv_max, 0.75*curv_max, 0.5*curv_max, 0.25*curv_max, 0.1*curv_max, -curv_max, -0.75*curv_max, -0.5*curv_max, -0.25*curv_max, -0.1*curv_max];
    for curv = curv_vals
        constraints.bv = [0; curv];
        constraints.bv_deriv = [0; 0];
        constraints.curv_rate = curv_rate_max;
        constraints.curv_max = curv_max;
        constraints.curv_rate_rate = curv_rate_rate_max;
        [C1, Sf1, flag1] = init_curv_poly_new(4, constraints);
        
        constraints.bv = [curv; 0];
        [C2, Sf2, flag2] = init_curv_poly_new(4, constraints);
        
        if flag1 && flag2
            data = [data; v, curv, Sf1, Sf2, C1, C2];
        else
            num_fail = num_fail + 1;
        end
    end
end

num_fail

fid = fopen('lut/curv_polys.txt', 'w');
fprintf(fid, '%d,%d,%f,%f,%f\n', length(v_vals), length(curv_vals), v_vals(1), v_vals(end), v_step);
fclose(fid);
dlmwrite('lut/curv_polys.txt', data, '-append');