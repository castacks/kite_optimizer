function s_air = get_airframe_distance(v_air, v_wind, s_ground, heading)
% returns the airframe distance (straight line).
% Assumes wind is along x-axis

    v_res = sqrt(v_air^2 + v_wind^2 + 2*v_air*v_wind*cos(heading));
    s_air = (s_ground / v_res) * v_air;
end 