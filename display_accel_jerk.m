function [fig1] = display_accel_jerk(route)
    accel = route.trajectory.a_air;
    jerk = route.trajectory.j_air;
    t = route.trajectory.t_vals;
    
    fig1 = figure;
    axes1 = axes('Parent',fig1,'YGrid','on','XGrid','on','ZColor',[0 0 0],...
        'YColor',[0 0 0],...
        'XColor',[0 0 0],...
        'GridAlpha', 0.25,...
        'FontWeight','bold',...
        'FontSize', 22, ...
        'YLim', [-route.model.max_accel - 0.2, route.model.max_accel + 0.2], ...
        'XLim', [0, t(end)] ...
    );
    box(axes1,'on');
    hold(axes1,'on');
    xlabel('Time (s)');
%     ylabel('Acceleration ((m/s^{2}) and Jerk (m/s^{3})');
    
    % plot accel and jerk
    plot(t, accel, 'b-', 'LineWidth', 4);
    plot(t, jerk, 'm-', 'LineWidth', 4);
    
    % plot limits
    r = refline(0, route.model.max_accel);
    r.LineWidth = 3;
    r.Color = 'r';
    r.LineStyle = '--';
    r = refline(0, -route.model.max_accel);
    r.LineWidth = 3;
    r.Color = 'r';
    r.LineStyle = '--';
    
    % add legend
    legend({'Acceleration (m/s^{2})', 'Jerk (m/s^{3})'}, 'Location','northoutside','Orientation','horizontal', 'Fontweight', 'bold', 'FontSize', 22)
end