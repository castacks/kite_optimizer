function [route, flag] = get_vel_profile(waypts, route, model, consts, allowed_err, wind_speed)
    
    %% Get segment limits
    [seg_s, seg_v, seg_a, seg_j, seg_maxV, route] = extract_seg_vel_limits(waypts, route, wind_speed, model);
    
    %% Define end-point conditions
    bv.init = [waypts(1, 4); 0];
    bv.end = [waypts(end, 4); 0];
    
%     %% Set up and initialize param vector
      X0 = velopt_init_fn(seg_v, seg_s, seg_maxV, bv, consts.ASSUMED_ACCEL);
    
    %% Set up upper and lower bounds
    lb = zeros(size(X0)) + max(wind_speed, 10);
    ub = seg_v(1:end-1);
    
    %% Set up cost and constraint function
    cost_func = @(x) vel_profile_cost_fn(x, seg_s, seg_v, seg_a, seg_j, seg_maxV, bv, consts, false);
    constr_func = @(x) vel_profile_constr_fn(x, seg_s, seg_v, seg_a, seg_j, seg_maxV, bv, consts);
    
    options = optimset('MaxFunEvals', 1000000, 'Display', 'off', 'MaxIter', 200);

    [X_opt, fval, exitflag, output] = fmincon(cost_func, X0, [], [], [], [], ...
        lb, ub, constr_func, options);
    
    flag = true;
    if exitflag ~= 1 && exitflag ~= 2 && exitflag ~= 3
        flag = false;
        return;
    end
    
    route.vels = X_opt;
    [route] = refine_vel_profile(route, route.vels, seg_s, seg_v, seg_a, seg_j, seg_maxV, bv, consts);
end