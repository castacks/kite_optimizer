function [ineq_con, eq_con] = vel_profile_constr_fn(X, seg_s, seg_v, seg_a, seg_j, seg_maxV, bv, consts)
%     [~, flags, errors] = get_vels_from_constraints(X, seg_s, seg_v, seg_a, seg_j, bv, consts);
% 
%     eq_con = [];
%     ineq_con = errors - (1e-4);
    [tf, feasible] = get_vels_from_bv(X, seg_s, seg_v, seg_maxV, bv, consts);
    eq_con = [];
    ineq_con = feasible;
end