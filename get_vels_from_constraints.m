function [tf, flags, errors] = get_vels_from_constraints(X, seg_s, seg_v, seg_a, seg_j, bv, consts)
%     X = [X; bv.end(1); bv.end(2)];
%     prev_bv = bv.init;
%     tf = zeros(size(seg_s));
%     flags = zeros(size(seg_s));
%     idx = 1;    
%     for i = 1:1:length(seg_s)
%         % Set up constraints for vel solver
%         constraints.bv = [prev_bv(1); X(idx)];
%         constraints.bv_deriv = [prev_bv(2); X(idx+1)];
%         constraints.bv_dderiv = 0;
%         constraints.bv_integral = seg_s(i);
%         constraints.speed_max = seg_v(i);
%         constraints.accel = seg_a(i);
%         constraints.jerk = seg_j(i);
%         
%         [~, tf(i), flags(i)] = init_vel_poly(consts.SPEED_POLY_DEG, constraints);
%         
%         prev_bv = [X(idx); X(idx+1)];
%         idx = idx + 2;       
%     end
    X = [X; bv.end(1)];
    prev_bv = bv.init;
    tf = zeros(size(seg_s));
    flags = zeros(size(seg_s));
    errors = zeros(size(seg_s));
    idx = 1;    
    for i = 1:1:length(seg_s)
        % Set up constraints for vel solver
        constraints.bv = [prev_bv(1); X(idx)];
        constraints.bv_deriv = [0; 0];
        constraints.bv_dderiv = 0;
        constraints.bv_integral = seg_s(i);
        constraints.speed_max = seg_v(i);
        constraints.accel = seg_a(i);
        constraints.jerk = seg_j(i);
        
        [~, tf(i), flags(i), errors(i)] = init_vel_poly(consts.SPEED_POLY_DEG, constraints);
        
        prev_bv = X(idx);
        idx = idx + 1;       
    end
end