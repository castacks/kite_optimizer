function [waypts] = capture_waypts(grid_size, grid_res)
    % generate grid
    [X, Y] = meshgrid(0:grid_res:grid_size, 0:grid_res:grid_size);
    
    % plot, and get points
    figure, hold on
    scatter(X(:), Y(:))
    waypts = [];
    while true
        [pt_x, pt_y] = ginput(1);
        waypts = [waypts; pt_x pt_y];
        plot(pt_x, pt_y, 'r*');
        k = waitforbuttonpress;
        if k == 1
            return
        end
    end
end