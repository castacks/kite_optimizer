function [turn, flag, iter_count] = get_naive_curv_turn(model, consts, allowed_err, heading1, heading2, wind_speed, iter_count)
    flag = false;
    
    if iter_count > consts.curvs.num_curv/2
        return;
    end

    %% Get initial and final curvatures headings (in the air-frame) for given ground-path headings
    curv_heading1 = get_heading_from_deriv(heading1, wind_speed, model.max_airspeed);
    curv_heading2 = get_heading_from_deriv(heading2, wind_speed, model.max_airspeed);

    %% Check for the degenerate case
    if abs(get_corr_heading_change(heading1, heading2)) < 1e-2
        turn.Sf1 = 0;
        turn.Sf2 = 0;
        turn.Sf3 = 0;
        turn.Sf_total = 0;
        turn.k_trans = 0;
        turn.X0 = [0 0];
        turn.curv_heading1 = curv_heading1;
        turn.curv_heading2 = curv_heading2;
%         turn.constraints1 = [];
%         turn.constraints3 = [];
        turn.empty = true;
        flag = true;
        return
    end
   
    
    %% Initialize
    if get_corr_heading_change(heading1, heading2) > 0
        k_high = model.curv_max;
        k_low = model.curv_max/20;
        positive = 1;
    else
        k_low = -model.curv_max;
        k_high = -model.curv_max/20;
        positive = 0;
    end
    
    if k_high > 0
        kvals = linspace(k_high, k_low, 20);
    else
        kvals = linspace(k_low, k_high, 20);
    end
    
    % Binary search to find the first feasible k_trans that
    % lets us achieve the given heading change
%     while(true)
%     for k_trans = kvals
%     for i = 1:consts.curvs.num_curv/2
    temp_iter_count = iter_count;
    for i = (consts.curvs.num_curv/2-iter_count+1):-1:1
        temp_iter_count = temp_iter_count + 1;
%         k_trans = (k_high + k_low) / 2;
%         if abs(k_trans - k_low) < 1e-4
%             break;
%         end
        
        % Get transition polys
%         constraints1.bv = [0; k_trans];
%         constraints1.bv_deriv = [0; 0];
%         constraints1.curv_rate = model.curv_rate_max;
%         constraints1.curv_max = model.curv_max;
%         constraints1.curv_rate_rate = model.curv_rate_rate_max;
%         constraints3.bv = [k_trans; 0];
%         constraints3.bv_deriv = [0; 0];
%         constraints3.curv_rate = model.curv_rate_max;
%         constraints3.curv_max = model.curv_max;
%         constraints3.curv_rate_rate = model.curv_rate_rate_max;
%         [C1, Sf1, flag1] = init_curv_poly_new(consts.CURV_POLY_DEG, constraints1);
%         [C3, Sf3, flag2] = init_curv_poly_new(consts.CURV_POLY_DEG, constraints3);
%         if ~(flag1 && flag2)
%             continue;
%         end
        
        [C1, C3, Sf1, Sf3, k_trans] = lookup_curv_splines(positive, i, model.max_airspeed, consts.curvs);
        
        [Sf2, rem_change] = get_initial_sf2(Sf1, C1, Sf3, C3, k_trans, curv_heading1, curv_heading2);
        
        % check if these turns achieve the desired heading change
        if rem_change >= 0
            % found a feasible turn
            turn.Sf1 = Sf1;
            turn.Sf2 = Sf2;
            turn.Sf3 = Sf3;
            turn.C1 = C1;
            turn.C2 = [zeros(1, consts.CURV_POLY_DEG) k_trans];
            turn.C3 = C3;
            turn.k_trans = k_trans;
            turn.X0 = [0 0];
            turn.curv_heading1 = curv_heading1;
            turn.curv_heading2 = curv_heading2;
%             turn.constraints1 = constraints1;
%             turn.constraints3 = constraints3;
            turn.empty = false;
            turn.limits = model;
            turn.Sf_total = Sf1 + Sf2 + Sf3;
            
            iter_count = temp_iter_count;
            flag = true;
            return;    
%             k_low = k_trans;
%         else
        end
%             k_high = k_trans;
        
    end
    
    iter_count = temp_iter_count;
    turn = [];
end