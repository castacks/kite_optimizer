function [] = plot_vel_polys(vel_polys)
    num = length(vel_polys);
    
    vels = [];
    a = [];
    j = [];
    tvals = [];
    prev_t = 0;
    way_vel = [];
    way_t = [];
    for i = 1:num
        prof = vel_polys{i};
        t = linspace(0, prof.tf1, 20);
        vels = [vels polyval(prof.vel_C1, t)];
        a = [a polyval(polyder(prof.vel_C1), t)];
        j = [j polyval(polyder(polyder(prof.vel_C1)), t)];
        t = t + prev_t;
        prev_t = t(end);
        tvals = [tvals t];
        way_vel = [way_vel polyval(prof.vel_C1, 0) polyval(prof.vel_C1, prof.tf1)];
        way_t = [way_t t(1) t(end)];

        if ~prof.empty
            t = linspace(0, prof.tf2, 20);
            vels = [vels polyval(prof.vel_C2, t)];
            a = [a polyval(polyder(prof.vel_C2), t)];
            j = [j polyval(polyder(polyder(prof.vel_C2)), t)];
            t = t + prev_t;
            prev_t = t(end);
            tvals = [tvals t];

            t = linspace(0, prof.tf3, 20);
            vels = [vels polyval(prof.vel_C3, t)];
            a = [a polyval(polyder(prof.vel_C3), t)];
            j = [j polyval(polyder(polyder(prof.vel_C3)), t)];
            t = t + prev_t;
            prev_t = t(end);
            tvals = [tvals t];
            way_vel = [way_vel polyval(prof.vel_C3, 0) polyval(prof.vel_C3, prof.tf1)];
            way_t = [way_t t(1) t(end)];
        end  
       
    end
    
    figure, plot(tvals, vels, 'r-', way_t, way_vel, 'b*'); xlabel('Time (s)'); ylabel('speed (m/s)');
    figure, plot(tvals, a, 'r-', tvals, j, 'b-'); xlabel('Time (s)');
    legend('Accel', 'Jerk');
end