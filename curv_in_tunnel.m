function [res] = curv_in_tunnel(tunnel, curv)
    res = zeros(size(curv, 1), 1);

    A = tunnel(1, :);
    B = tunnel(2, :);
    C = tunnel(3, :);
    
    dot_A_B = dot(B-A, B-A);
    dot_B_C = dot(C-B, C-B);
    for i = 1:size(curv, 1)
        pt = curv(i, :);
        temp1 = dot(B-A, pt-A);
        temp2 = dot(C-B, pt-B);
        
        if temp1 < 0 || temp2 < 0 || temp1 > dot_A_B || temp2 > dot_B_C
            res(i) = 0;
        else
            res(i) = 1;
        end
    end
end