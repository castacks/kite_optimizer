function [dist] = point_dist_from_line(P, P1, P2)
    dist = ((P2(2) - P1(2))*P(1) - (P2(1) - P1(1))*P(2) + P2(1)*P1(2) - P2(2)*P1(1)) / ...
        norm(P2 - P1);
    dist = abs(dist);
end