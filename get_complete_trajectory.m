function [route, flag] = get_complete_trajectory(waypts, model, wind_speed, allowed_err, consts)

    waypts_new = waypts;
    
    %% Read accel and curv LUT
    [consts.accels, consts.curvs] = read_accel_curv_polys(...
        'lut/accel_polys.txt', 'lut/curv_polys.txt');

    %% Get turns
    num_turns = size(waypts_new, 1) - 2;
    route.turns = cell(num_turns, 1);
    fprintf('************* Computing ground-frame path *************\n');
    for i = 1:num_turns
        [route.turns{i}, flag] = fit_poly_to_wayline(waypts_new(i:i+2, :), ...
            model, consts, allowed_err, wind_speed);
        if ~flag
            fprintf('Failing for turn %d', i);
            return;
        end
    end
    
    %% Get airspeed profile
    fprintf('************* Computing airspeed profile *************\n');
    [route, flag] = get_vel_profile(waypts_new, route, model, consts, ...
        allowed_err, wind_speed);
    if ~flag
        fprintf('Failed to compute airspeed profile!!');
        return;
    end
    
    %% Get trajectory
    fprintf('************* Computing final trajectory *************\n');
    [route, flag] = construct_trajectory(route, wind_speed, ...
        consts, waypts_new, model);
    
    route.waypts = waypts;
    route.model = model;
end