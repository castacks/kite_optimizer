function [path_X, path_Y, path_heading, path_speed, path_bank, path_curv, path_times, path_deriv] = ...
    get_path(X, wind_speed, waypoints, heading1, heading2, consts, model)
    %% Set up polynomials
    X_curv = X(1:consts.CURV_POLY_DEG);
    X_speed = [];
    X_P1 = [waypoints(1, 1) waypoints(1, 2)];
    Sf = X(end);
    curv_poly = [X_curv; 0]';
    speed_poly = [X_speed; waypoints(1, 3)]';
    
    %% get heading function
    psi = polyint(curv_poly, heading1);
    fun_X = @(x) cos(wrapTo2Pi(polyval(psi, x)));
    fun_Y = @(x) sin(wrapTo2Pi(polyval(psi, x)));
    
    %% create sample grid
    samples = transpose(linspace(0, Sf, consts.NUM_SAMPLES));
    
    %% Evaluate sample times and path
    time_fn = @(x) (polyval(speed_poly, x)).^-1;
    
    path_times = zeros(size(samples));
    path_X = zeros(size(samples));
    path_Y = zeros(size(samples));
    path_heading = zeros(size(samples));
    path_speed = zeros(size(samples));
    path_bank = zeros(size(samples));
    path_curv = zeros(size(samples));
    path_deriv = zeros(size(samples));
    
    path_heading(1) = polyval(psi, samples(1));
    path_speed(1) = polyval(speed_poly, samples(1));
    for i = 2:length(samples)
        vel = polyval(speed_poly, samples(i));
        path_times(i) = path_times(i-1) + integral(time_fn, samples(i-1), samples(i));
        path_heading(i) = polyval(psi, samples(i));
        path_speed(i) = vel;
        path_curv(i) = polyval(curv_poly, samples(i));
        path_X(i) = integral(fun_X, 0, samples(i)) + path_times(i) * wind_speed;
        path_Y(i) = integral(fun_Y, 0, samples(i));
        path_bank(i) = wrapToPi(atan2(path_speed(i)*path_speed(i)*path_curv(i), model.g));
        path_deriv(i) = vel * sin(path_heading(i)) / (vel * cos(path_heading(i)) + wind_speed);
    end
    path_X = path_X + X_P1(1);
    path_Y = path_Y + X_P1(2);
    path_deriv = diff(path_Y) ./ diff(path_X);
    
    path_heading = wrapToPi(path_heading);
end