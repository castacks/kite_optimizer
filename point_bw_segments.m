function [flag] = point_bw_segments(P, X1, X2, X3)
% Checks if P is on the same side of line segments defined by the given
% vertices
    sgn = cross([X2-X1 0], [X3-X2 0]);
    sgn = sign(sgn(3));
    
    temp = cross([X3-X2 0], [P-X2 0]);
    flag = (sign(temp(3)) == sgn);
end