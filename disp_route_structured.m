function [] =  disp_route_structured(X, wind_speed, waypoints, curv_heading1, heading2, consts, model, constraints1, constraints3)
    [path_X, path_Y, path_heading, path_speed, path_bank, path_curv, path_times, path_deriv, roll_rate] = ...
        get_path_structured(X, wind_speed, waypoints, curv_heading1, heading2, consts, model, constraints1, constraints3);
    
    %% Path
    figure, hold on;
    plot(path_X, path_Y, 'g-');
%     plot(waypoints(:, 1), waypoints(:, 2), 'r-');
    plot(path_X(1), path_Y(1), 'b*');
    plot(path_X(end), path_Y(end), 'k*');
    legend('Path', 'Linear track', 'Waypoints');
    axis equal;
    
    %% Roll angle
    figure, hold on
    plot(path_times, rad2deg(path_bank), 'r-');
    refline(0, rad2deg(model.max_roll));
    refline(0, -rad2deg(model.max_roll));
    legend('Roll angle (deg)', 'Max roll angle');
    
    %% Roll rate
    figure, plot(1:length(roll_rate), rad2deg(roll_rate), 'r-'); hold on;
    refline(0, rad2deg(model.max_roll_rate));
    refline(0, -rad2deg(model.max_roll_rate));
    legend('Roll-rate (deg/s)', 'Max roll-rate');
    
    %% Path derivatives
    figure, hold on
    plot(path_times, rad2deg(atan2(path_deriv(:, 2), path_deriv(:,1))), 'b-');
end