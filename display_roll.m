function [fig1] = display_roll(route)
    roll = route.trajectory.actual_roll;
    roll_rate = route.trajectory.actual_roll_rate;
    roll_ddt = route.trajectory.actual_roll_ddt;
    t = route.trajectory.t_vals;
    
    fig1 = figure;
    axes1 = axes('Parent',fig1,'YGrid','on','XGrid','on','ZColor',[0 0 0],...
        'YColor',[0 0 0],...
        'XColor',[0 0 0],...
        'GridAlpha', 0.25,...
        'FontWeight','bold',...
        'FontSize', 22, ...
        'YLim', [-(rad2deg(route.model.max_roll)+5), rad2deg(route.model.max_roll)+5], ...
        'XLim', [0, t(end)] ...
    );
    box(axes1,'on');
    hold(axes1,'on');
    xlabel('Time (s)');
%     ylabel('Roll (deg/s), Roll-rate (deg/s^{2}). Roll-rate-rate (deg/s^{3})');
    
    % plot
    plot(t, rad2deg(roll), 'b-', 'LineWidth', 3);
    plot(t, rad2deg(roll_rate), 'm-', 'LineWidth', 3);
    plot(t, rad2deg(roll_ddt), 'k-', 'LineWidth', 2);
    
    % legend
    legend({'$\mathbf{{\phi}}$ (deg)', '$\dot{\mathbf{\phi}}$ (deg/s)', '$\ddot{\mathbf{\phi}}$ (deg/$s^{2}$)'}, ...
        'Interpreter', 'Latex', 'Location','northoutside','Orientation','horizontal', 'Box', 'off', 'Fontweight', 'bold', 'FontSize', 24);
end