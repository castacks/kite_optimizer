function [curv_poly_params, Sf, flag] = get_curv_poly(heading1, heading2, consts, model, allowed_err, wind_speed, waypoints)
% Returns a curvature polynomial that effects a heading change from
% heading1 to heading2.
% flag is set to true if a curvature polynomial was successfuly found,
% false otherwise.

    curv_poly_params = [];
    Sf = 0;
    flag = false;

    %% Initialize curvature polynomial
    curv_cons.curv_ep = 0;
    curv_cons.heading_ep = heading2 - heading1;
    curv_cons.curv_deriv_ep = 0;
    curv_cons.curv_rate = model.curv_rate_max;
    curv_cons.curv_max = model.curv_max;
    [X_kappa, X_Sf, flag] = init_curv_poly(consts.CURV_POLY_DEG, curv_cons);
    
    if flag
        %% Set up params for the optimizer
        X0 = [X_kappa; X_Sf];
        lb = [-5 *ones(consts.CURV_POLY_DEG, 1); 0.1 * X_Sf];
        ub = [5 *ones(consts.CURV_POLY_DEG, 1); X_Sf*5];
        cost_func = @(x) cost_fn(x, wind_speed, waypoints, heading1, heading2, consts, model);
        constr_func = @(x) constraint_fn(x, wind_speed, waypoints, heading1, heading2, consts, model, allowed_err);
        options = optimset('MaxFunEvals', 1000000, 'Display', 'iter', 'TolFun', 1e-2,'MaxIter', 200);

        [X_opt, fval, exitflag, output] = fmincon(cost_func, X0, [], [], [], [], ...
            lb, ub, constr_func, options);

        curv_poly_params = [X_opt(1:consts.CURV_POLY_DEG); 0]';
        Sf = X_opt(end);
        flag = (exitflag == 1);
    end
end