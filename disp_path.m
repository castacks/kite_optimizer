function [] = disp_path(X, wind_speed, waypoints, heading1, heading2, consts, model)
    [path_X, path_Y, path_heading, path_speed, path_bank, path_curv, path_times, path_deriv] = ...
        get_path(X, wind_speed, waypoints, heading1, heading2, consts, model);
    
    %% Path
    figure, hold on;
    plot(path_X, path_Y, 'g-');
%     plot(waypoints(:, 1), waypoints(:, 2), 'r-');
    plot(path_X(1), path_Y(1), 'b*');
    plot(path_X(end), path_Y(end), 'k*');
    legend('Path', 'Linear track', 'Waypoints');
    
    %% Roll angle
    figure, hold on
    plot(path_times, rad2deg(path_bank), 'r-');
    refline(0, rad2deg(model.max_roll));
    refline(0, -rad2deg(model.max_roll));
    legend('Roll angle (deg)', 'Max roll angle');
    
    %% Roll rate
    roll_rate = diff(path_bank) ./ diff(path_times); 
    figure, plot(path_times(2:end), rad2deg(roll_rate), 'r-'); hold on;
    refline(0, rad2deg(model.max_roll_rate));
    refline(0, -rad2deg(model.max_roll_rate));
    legend('Roll-rate (deg/s)', 'Max roll-rate');
    
    %% Path derivatives
    figure, hold on
    plot(path_times(2:end), path_deriv, 'b-');
end