function [route] = rotate_trajectory(route, R)
    % Rotates the trajectory back into the inertial frame.
    % R -> rotation matrix used to transform input waypoints
    path = [route.trajectory.X; route.trajectory.Y; route.trajectory.Z];
    path = R' * path;
    route.trajectory.X = path(1, :)';
    route.trajectory.Y = path(2, :)';
    route.trajectory.Z = path(3, :)';
end