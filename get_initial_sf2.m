function [Sf2, rem_change] = get_initial_sf2(Sf1, C1, Sf3, C3, k_trans, curv_heading1, curv_heading2)
    psi1 = polyint(C1, 0);
    psi3 = polyint(C3, 0);
    heading_change = polyval(psi1, Sf1) + polyval(psi3, Sf3);
    rem_change = abs(get_corr_heading_change(curv_heading1, curv_heading2)) - abs(heading_change);
    
    if rem_change < 0
        Sf2 = eps;
    else
        Sf2 = abs(rem_change / k_trans);
    end
end