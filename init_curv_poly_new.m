function [curv_poly_coeffs, Sf, flag] = init_curv_poly_new(degree, constraints)
    %% Constants
    Sf_min = 20;
    Sf_max = 2000;
    Sf_vals = Sf_min:20:Sf_max;
    
    idx = 1;
    flag = false;
    while true
        Sf = Sf_vals(idx);
        [curv_poly_coeffs, flag] = get_poly_from_sf(degree, constraints, Sf, true);
        
        %% Check if we've found a satisfactory solution
        if flag
            break;
        else
            idx = idx + 1;
            if idx > length(Sf_vals)
                flag = false;
                return
            end
        end
    end
end