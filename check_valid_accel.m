function [tf2, Sf_used, vel_C1, vel_C2, vel_C3, flag] = ...
            check_valid_accel(tf1, C1, tf3, C3, a_trans, vel_init, vel_final, Sf, consts)
    
    vel_C1 = polyint(C1, 0);
    vel_C3 = polyint(C3, 0);
    change = abs(polyval(vel_C1, tf1)) + abs(polyval(vel_C3, tf3));
    rem_change = abs(vel_final - vel_init) - change;
    
    if rem_change < 0
        flag = false;
        tf2 = -1;
        Sf_used = -1;
        vel_C2 = [];
        return;
    end
    
    tf2 = abs(rem_change / a_trans);
    vel_C1(end) = vel_init;
    vel_C2 = polyint(a_trans, polyval(vel_C1, tf1));
    vel_C3(end) = polyval(vel_C2, tf2);
    
    % Equalize poly vector lengths
    vel_C1 = [zeros(1,consts.ACCEL_POLY_DEG+2-length(vel_C1)) vel_C1];
    vel_C2 = [zeros(1,consts.ACCEL_POLY_DEG+2-length(vel_C2)) vel_C2];
    vel_C3 = [zeros(1,consts.ACCEL_POLY_DEG+2-length(vel_C3)) vel_C3];
    
    Sf_used = polyval(polyint(vel_C1), tf1) + polyval(polyint(vel_C2), tf2) ...
        + polyval(polyint(vel_C3), tf3);
    
    if Sf_used > Sf
        flag = false;
    else
        flag = true;
    end
end