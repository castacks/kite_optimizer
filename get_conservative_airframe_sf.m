function [s_air, v_air, heading] = get_conservative_airframe_sf(v1, v2, vw, s_ground, theta_des)
% v1, v2 -> candidate segment velocities
% S -> ground-track distance
% vw -> wind speed
    if v1 == 0
        S1 = Inf;
    else
        heading1 = get_heading_from_deriv(theta_des, vw, v1);
        S1 = get_airframe_distance(v1, vw, s_ground, heading1);
    end
    
    if v2 == 0
        S2 = Inf;
    else
        heading2 = get_heading_from_deriv(theta_des, vw, v2);
        S2 = get_airframe_distance(v2, vw, s_ground, heading2);
    end
    
    if S1 < S2
        s_air = S1;
        v_air = v1;
        heading = heading1;
    else
        s_air = S2;
        v_air = v2;
        heading = heading2;
    end
    
end