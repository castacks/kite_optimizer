function [fig1] = display_path(route)
    X = route.trajectory.X;
    Y = route.trajectory.Y;
    waypts = route.waypts;

    fig1 = figure;
    axes1 = axes('Parent',fig1,'YGrid','on','XGrid','on','ZColor',[0 0 0],...
        'YColor',[0 0 0],...
        'XColor',[0 0 0],...
        'GridAlpha', 0.0,...
        'FontWeight','bold',...
        'FontSize', 22, ...
        'DataAspectRatio', [1, 1, 1],...
        'XLim', [min(waypts(:, 1)) - 500, max(waypts(:, 1)) + 500], ...
        'YLim', [min(waypts(:, 2)) - 500, max(waypts(:, 2)) + 500] ...
        );
    box(axes1,'on');
    hold(axes1,'on');
    xlabel('X (m)');
    ylabel('Y (m)');
    
    % Plot corridor
    if size(waypts, 2) > 4
        for i = 1:size(waypts, 1)-1
            verts = generate_corridor([waypts(i, 1), waypts(i, 2)], ...
                [waypts(i+1, 1), waypts(i+1, 2)], waypts(i, 5), waypts(i, 6));
            f = fill3(verts(:, 1), verts(:, 2), zeros(4, 1), 'y-');
            f(1).FaceAlpha = 0.5;
            f(1).EdgeAlpha = 0.1;
        end
    end
    
    % Plot path
    plot(X, Y, 'k-', 'LineWidth', 4);
    
    % Plot waypoints
%     plot(waypts(2:end-1, 1), waypts(2:end-1, 2), 'b*', 'MarkerSize', 10, 'LineWidth', 2);
    plot(X(1), Y(1), 'r*', 'MarkerSize', 12,  'LineWidth', 2);
    plot(X(end), Y(end), 'g*', 'MarkerSize', 12,  'LineWidth', 2);
    
    % Add title
%     title('Route length: 291.3 km');
end