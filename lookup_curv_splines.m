function [C1, C2, Sf1, Sf2, ktrans] = lookup_curv_splines(positive, count, vel, curvs)
    vel_idx = (vel - curvs.min_vel) / curvs.vel_step * curvs.num_curv;
    if positive == 1
        idx = vel_idx + count;
    else 
        idx = vel_idx + curvs.num_curv/2 + count;
    end
    
    C1 = curvs.curv_list{idx}.c1;
    C2 = curvs.curv_list{idx}.c2;
    Sf1 = curvs.curv_list{idx}.Sf1;
    Sf2 = curvs.curv_list{idx}.Sf2;
    ktrans = curvs.curv_list{idx}.ktrans;
end

