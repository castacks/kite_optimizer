function [] = display_trajectory(traj, consts, waypts, wind_speed)
    % Display corridors and path
    figure; hold on
    if size(waypts, 2) > 4
        for i = 1:size(waypts, 1)-1
            verts = generate_corridor([waypts(i, 1), waypts(i, 2)], ...
                [waypts(i+1, 1), waypts(i+1, 2)], waypts(i, 5), waypts(i, 6));
            f = fill3(verts(:, 1), verts(:, 2), zeros(4, 1), 'y-');
            f(1).FaceAlpha = 0.5;
            f(1).EdgeAlpha = 0.1;
        end
    end

    plot3(waypts(:, 1), waypts(:, 2), -waypts(:, 3), 'k*');
    plot3(traj.X(1), traj.Y(1), -traj.Z(1), 'r*');
    plot3(traj.X(end), traj.Y(end), -traj.Z(end), 'g*');
%     legend('Waypoints', 'Start', 'Goal');
    plot3(traj.X, traj.Y, -traj.Z, 'b-');
    tt = sprintf('Ground-frame 3D path (m). Wind speed %d m/s', wind_speed);
%     title(tt);
    axis equal;
    
    % Display groundspeed and airspeed
    figure, hold on;
%     plot((0:length(trajectory.X_dot)-1) * consts.TIME_STEP, ...
%         sqrt(trajectory.X_dot.^2 + trajectory.Y_dot.^2), 'b-');
    plot(traj.t_vals, traj.airspeed, 'r-');
    plot(traj.t_vals, traj.groundspeed, 'g-');
    legend('Airspeed (m/s)', 'Groundspeed (m/s)');
%     line([traj.t_vals(traj.breaks); traj.t_vals(traj.breaks)], get(gca, 'ylim'), ...
%         'LineStyle', '--');
    xlabel('Time (s)');
    tt = sprintf('Airspeed and groundspeed. Wind speed %d m/s', wind_speed);
    title(tt);
    
    % Display ground-referred accel and jerk
    figure, hold on
%     plot((0:length(trajectory.X_ddot)-1) * consts.TIME_STEP, ...
%         sqrt(trajectory.X_ddot.^2 + trajectory.Y_ddot.^2), 'b--');
%     plot((0:length(trajectory.X_dddot)-1) * consts.TIME_STEP, ...
%         sqrt(trajectory.X_dddot.^2 + trajectory.Y_dddot.^2), 'r--');
    plot((0:length(traj.accel)-1) * consts.TIME_STEP, ...
        traj.accel, 'g-');
    plot((0:length(traj.jerk)-1) * consts.TIME_STEP, ...
        traj.jerk, 'k-');
    legend('Acceleration (m/s^2) w.r.t ground', 'Jerk (m/s^3) w.r.t. ground');
    xlabel('Time (s)');
    tt = sprintf('Acceleration and Jerk. Wind speed %d m/s', wind_speed);
    title(tt);
    
    % Roll and roll-rate
    figure, hold on;
    plot((0:length(traj.actual_roll)-1) * consts.TIME_STEP, ...
        rad2deg(traj.actual_roll), 'r-');
    plot((0:length(traj.actual_roll_rate)-1) * consts.TIME_STEP, ...
        rad2deg(traj.actual_roll_rate), 'b-');
    legend('Roll (deg)', 'Roll-rate (deg/s)');
    xlabel('Time (s)');
    tt = sprintf('Roll and roll-rate. Wind speed %d m/s', wind_speed);
    title(tt);
    
%     line([traj.t_vals(traj.breaks); traj.t_vals(traj.breaks)], get(gca, 'ylim'), ...
%         'LineStyle', '--');
end