function [tf, feasible] = get_vels_from_bv(X, seg_s, seg_v, seg_maxV, bv, consts)
    X = [X; bv.end(1)];
    a = consts.ASSUMED_ACCEL;
    prev_bv = bv.init(1);
    tf = zeros(size(seg_s));
    feasible = zeros(size(seg_s));
    
    for i = 1:length(seg_s)
        [tf(i), feasible(i)] = const_accel_vel_prof(prev_bv, X(i), seg_s(i), a, seg_maxV(i));
        prev_bv = X(i);
    end
end