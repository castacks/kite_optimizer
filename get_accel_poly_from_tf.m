function [coeffs, flag] = get_accel_poly_from_tf(degree, constraints, tf, init)
    if nargin < 4
        init = true;
    end
    
    err_tol = 1e-5;
    num_samples = 100;
    flag = false;
    
    %% Set up LHS
    % boundary values
    C_bv = [];
    if ~isempty(constraints.bv)
        C_bv = zeros(2, degree+1);
        for i = degree:-1:0
            C_bv(1, degree-i+1) = 0 ^ i;
            C_bv(2, degree-i+1) = tf ^ i;
        end
    end

    % boundary derivative values
    C_bv_deriv = [];
    if ~isempty(constraints.bv_deriv)
        C_bv_deriv = zeros(2, degree+1);
        for i = degree:-1:1
            C_bv_deriv(1, degree-i+1) = i * 0 ^ (i-1);
            C_bv_deriv(2, degree-i+1) = i * tf ^ (i-1);
        end
    end

    C = [C_bv; C_bv_deriv];

    %% Set up RHS
    d = [constraints.bv; constraints.bv_deriv];
    
    if init
        %% Set up inequality constraints
        samples = transpose(linspace(0, tf, num_samples));

        % Accel constraint
        A_accel = [];
        for i = degree:-1:0
            A_accel = [A_accel [samples.^i; -samples.^i]];
        end
        b_accel = zeros(size(A_accel, 1), 1) + constraints.accel;

        % Jerk constraint
        A_jerk = [];
        for i = degree:-1:1
            A_jerk = [A_jerk [i * samples.^(i-1); -i * samples.^(i-1)]]; 
        end
        A_jerk = [A_jerk zeros(2*length(samples), 1)];
        b_jerk = zeros(size(A_jerk, 1), 1) + constraints.jerk;

        A = [A_accel; A_jerk];
        b = [b_accel; b_jerk];
        
        % Optimize!
        options = optimset('MaxIter', 500, 'Algorithm', 'active-set');
        [coeffs, resnorm,residual,exitflag,output,lambda] = lsqlin(C, d, A, b, [], [], [], [], [], options);
        coeffs = coeffs';
        
        % Check if we've found a satisfactory solution
        if max(abs(residual)) < err_tol && output.constrviolation < eps
            flag = true;
            return;
        end
    end
    
    coeffs = (C \ d)';
end