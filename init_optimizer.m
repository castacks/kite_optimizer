function [model, consts, allowed_err] = init_optimizer()
    % Note -> The lookup tables have been generated according to the limits
    % defined here.


    %% Model
    model.g = 9.81;
    model.max_airspeed = 50;    % m/s
    model.min_airspeed = 20;
    model.max_roll = deg2rad(15);
    model.max_roll_rate = deg2rad(15);
    model.max_roll_rate_rate = deg2rad(15);
    model.max_accel = 0.1 * model.g;
    model.max_jerk = 0.1 * model.g;
    model.curv_max = model.g * tan(model.max_roll) / (model.max_airspeed^2);
    model.curv_rate_max = model.g * model.max_roll_rate / (model.max_airspeed^3) - 2 * model.curv_max * model.max_accel / (model.max_airspeed^2);
    model.curv_rate_rate_max = model.g * model.max_roll_rate_rate / (model.max_airspeed^4);
    model.max_vel_z = 1000 * 0.00508;    

    %% constants
    consts.CURV_POLY_DEG = 4;
    consts.SPEED_POLY_DEG = 4;
    consts.ACCEL_POLY_DEG = 3 ;
    consts.NUM_SAMPLES = 100;
    consts.TIME_WEIGHT = 1.0;
    consts.CURV_SMOOTH_WEIGHT   = 100.0;
    consts.NUM_SEGMENT_SAMPLES = 25;
    consts.g = 9.81;
    consts.TIME_STEP = 0.1;
    consts.ASSUMED_ACCEL = 0.4 * model.max_accel;
    
    %% allowed errors (deprecated, ignore)
    allowed_err.curv = 0.0001;
    allowed_err.heading = deg2rad(1);
    allowed_err.speed = 5;
    allowed_err.pos = 1;
    allowed_err.dydx = 0.005;
end