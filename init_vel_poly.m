function [vel_poly] = init_vel_poly(degree, constraints, max_vel, min_vel)
    if nargin < 4
        max_vel = 60;
        min_vel = 10;
    end
    
    vel_poly.init = constraints.bv(1);
    vel_poly.end = constraints.bv(2);
    vel_poly.Sf = constraints.bv_integral;
    
    %% Check if we want a constant vel profile
    if constraints.straight
        vel_poly.coeffs = constraints.bv(1);
        vel_poly.error = 0;
        vel_poly.flag = true;
        vel_poly.tf = constraints.bv_integral / constraints.bv(1);
        vel_poly.init = constraints.bv(1);
        vel_poly.end = constraints.bv(1);
        vel_poly.Sf = constraints.bv_integral;
        return;
    end

    %% Constants
    tf_min = floor(constraints.bv_integral / max_vel);
    tf_max = 500;
    tf_vals = tf_min:5:tf_max;
    
    flag = false;
    best_err = 1e10;
    for tf = tf_vals
        [vel_poly_coeffs, flag, error] = get_velpoly_from_tf(degree, constraints, tf, true);
        
        if error < best_err
            best_err = error;
        end
        
        %% Check if we've found a satisfactory solution
        if flag
%             temp = sprintf('Found vel with time %d', tf);
%             display(temp);
            vel_poly.tf = tf;
            vel_poly.coeffs = vel_poly_coeffs;
            vel_poly.error = error;
            vel_poly.flag = flag;
            return;
        end
    end
    
    vel_poly.flag = flag;
    vel_poly.error = best_err;
end