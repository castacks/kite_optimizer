function [dh] = get_heading_dot(heading, time_vals)
    heading = heading(:);
    time_vals = time_vals(:);
    if length(time_vals) > 1
        time_vals = diff(time_vals);
        time_vals = time_vals(1:end-1);
    end

    X1 = heading(3:end);
    X2 = heading(1:end-2);
    dh = zeros(size(X1));
    for i = 1:length(X1)
        dh(i) = 0.5 * get_corr_heading_change(X2(i), X1(i));
    end
    dh = dh ./ time_vals;
    
    if size(dh, 2) ~= 1
        dh = dh';
    end
    dh = [0; dh; 0];
end