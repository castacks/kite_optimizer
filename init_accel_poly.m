function [C, tf, flag] = init_accel_poly(degree, constraints)
    tf_min = floor(abs(diff(constraints.bv)) / constraints.jerk);
    tf_max = 200;
    tf_vals = tf_min:2:tf_max;
    
    for tf = tf_vals
        [C, flag] = get_accel_poly_from_tf(degree, constraints, tf, true);
        if flag
            return;
        end
    end
    
    C = [];
    flag = false;
end