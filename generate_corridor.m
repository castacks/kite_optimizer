function [verts] = generate_corridor(pt1, pt2, left, right)
    % Get perpendicular vectors
    vect = pt2 - pt1;
    dx = vect(1); dy = vect(2);
    left_vect = [-dy, dx] / norm(vect) * left; 
    right_vect = [dy, -dx] / norm(vect) * right;
    
    % Get vertices of the rectangle
    verts = [left_vect+pt1; left_vect+pt2; right_vect+pt2; right_vect+pt1];
end