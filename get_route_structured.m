clear; clc; 
%% constants
consts.CURV_POLY_DEG = 4;
consts.SPEED_POLY_DEG = 4;
consts.ACCEL_POLY_DEG = 3 ;
consts.NUM_SAMPLES = 100;
consts.TIME_WEIGHT = 1.0;
consts.CURV_SMOOTH_WEIGHT = 10000.0;
consts.NUM_SEGMENT_SAMPLES = 25;
consts.ASSUMED_ACCEL = 0.25 * 0.981;
consts.g = 9.81;
consts.TIME_STEP = 0.03;

%% allowed errors (not used right now with the least-squares stuff)
allowed_err.curv = 0.0001;
allowed_err.heading = deg2rad(1);
allowed_err.speed = 5;
allowed_err.pos = 1;
allowed_err.dydx = 0.005;

%% Model
model.g = 9.81;
model.max_airspeed = 30;    % m/s
model.max_roll = deg2rad(15);
model.max_roll_rate = deg2rad(10);
model.max_roll_rate_rate = deg2rad(30);
model.max_accel = 0.1 * model.g;
model.max_jerk = 0.1 * model.g;
model.curv_max = model.g * tan(model.max_roll) / (model.max_airspeed^2);
model.curv_rate_max = model.g * model.max_roll_rate / (model.max_airspeed^3);
model.curv_rate_rate_max = model.g * model.max_roll_rate_rate / (model.max_airspeed^4);

%% Inputs
% waypoints = csvread('route.csv'); % assume x-axis is aligned with wind
waypoints = get_route_data();
wind_speed = 0;  % m/s

%% Parse route
angles = normr(diff(waypoints(:, 1:2)));
angles = atan2(angles(:, 2), angles(:, 1));
angles = wrapToPi(angles);

%% Get curvature polynomial
% heading1 = angles(1);
% heading2 = angles(2);
dydx1 = 0;
dydx2 = pi/2;
[turn, flag] = ...
    get_structured_poly(model, consts, allowed_err, waypoints, dydx1, dydx2, wind_speed);
% 
% [turn, flag] = ...
%     get_naive_curv_turn(model, consts, allowed_err, waypoints(4:6, :), dydx1, dydx2, wind_speed);