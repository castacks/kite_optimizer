function [seg_s, seg_v, seg_a, seg_j, seg_maxV, route] = extract_seg_vel_limits(waypts, route, wind_speed, model)
    seg_s = []; seg_v = []; seg_a = []; seg_j = []; seg_maxV = [];
    
    % extract segment limits
    prev_pt = waypts(1, 1:2);
    prev_seg_vel = waypts(1, 4);
    augmented_turns = cell(length(route.turns)+size(waypts, 1)-1, 1); idx = 1;
    cum_sf = 0;
    waypts_map = zeros(size(waypts, 1), 1);  % indexes which turns map to which waypt
    for i = 1:size(waypts, 1)-2
        turn = route.turns{i};
        
        % For straight line segment before turn
%         seg_s = [seg_s; norm(prev_pt - turn.X0)];
        if turn.empty
            seg_v = [seg_v; waypts(i+1, 4)];
            seg_maxV = [seg_maxV; waypts(i+1, 4)];
            seg_a = [seg_a; model.max_accel];
            seg_j = [seg_j; model.max_jerk];
        else
            seg_v = [seg_v; turn.limits.max_airspeed];
            seg_maxV = [seg_maxV; waypts(i+1, 4)];
            seg_a = [seg_a; model.max_accel];
            seg_j = [seg_j; model.max_jerk];
        end
        theta_des = turn.X0 - prev_pt;
        theta_des = wrapTo2Pi(atan2(theta_des(2), theta_des(1)));
%         [s_air, v_air, heading_air] = get_conservative_airframe_sf(...
%             prev_seg_vel, seg_v(end), wind_speed, norm(prev_pt - turn.X0), theta_des);
%         seg_s = [seg_s; s_air];
%         strght.limits.max_airspeed = v_air;
%         strght.X0 = prev_pt;
%         strght.curv_pp = mkpp([0; s_air], zeros(1,consts.CURV_POLY_DEG+1));
%         strght.heading_pp = mkpp([0; s_air], [zeros(1,consts.CURV_POLY_DEG+1) heading_air]);
        strght = generate_straight_segment(prev_seg_vel, seg_v(end), ...
            norm(prev_pt - turn.X0), prev_pt, theta_des, wind_speed);
        seg_s = [seg_s; strght.s_air];
        strght.Sf_total = strght.s_air; strght.Sf_start = cum_sf;
        cum_sf = cum_sf + strght.Sf_total;
        waypts_map(i+1) = idx;
        augmented_turns{idx} = strght; 
        idx = idx + 1;
                    
        % For turn
        if ~turn.empty
            seg_s = [seg_s; turn.Sf1+turn.Sf2+turn.Sf3];
            seg_v = [seg_v; turn.limits.max_airspeed];
            seg_a = [seg_a; model.max_accel];
            seg_j = [seg_j; model.max_jerk];
            seg_maxV = [seg_maxV; turn.limits.max_airspeed];
            turn.Sf_start = cum_sf;
            cum_sf = cum_sf + turn.Sf_total;
            turn.straight = false;
            augmented_turns{idx} = turn;
            waypts_map(i+1) = idx;
            idx = idx + 1; 
        end
        
        prev_seg_vel = seg_v(end);
        prev_pt = turn.end;
    end
    % Add last segment
    theta_des = waypts(end, 1:2) - prev_pt;
    theta_des = wrapTo2Pi(atan2(theta_des(2), theta_des(1)));
%     [s_air, v_air, heading_air] = get_conservative_airframe_sf(...
%             prev_seg_vel, seg_v(end), wind_speed, norm(prev_pt - waypts(end, 1:2)), theta_des);
%     strght.limits.max_airspeed = v_air;
%     strght.X0 = prev_pt;
%     strght.curv_pp = mkpp([0; s_air], zeros(1,consts.CURV_POLY_DEG+1));
%     strght.heading_pp = mkpp([0; s_air], [zeros(1,consts.CURV_POLY_DEG+1) heading_air]);
%     strght.Sf_total = s_air; strght.Sf_start = cum_sf;
    strght = generate_straight_segment(prev_seg_vel, seg_v(end), ...
        norm(prev_pt - waypts(end, 1:2)), prev_pt, theta_des, wind_speed);
    strght.Sf_total = strght.s_air; strght.Sf_start = cum_sf;
    cum_sf = cum_sf + strght.Sf_total;
    augmented_turns{idx} = strght;
    seg_s = [seg_s; strght.s_air];
    seg_v = [seg_v; waypts(end-1, 4)];
    seg_a = [seg_a; model.max_accel];
    seg_j = [seg_j; model.max_jerk];
    seg_maxV = [seg_maxV; waypts(end-1, 4)];
    
    augmented_turns = augmented_turns(1:idx);
    waypts_map(end) = idx;
    
    route.augmented_turns = augmented_turns;
    route.total_Sf = cum_sf;
    route.waypts_map = waypts_map;
end