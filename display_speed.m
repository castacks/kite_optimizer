function [fig1] = display_speed(route)
    speed = route.trajectory.airspeed;
    t = route.trajectory.t_vals;
    
    fig1 = figure;
    axes1 = axes('Parent',fig1,'YGrid','on','XGrid','on','ZColor',[0 0 0],...
        'YColor',[0 0 0],...
        'XColor',[0 0 0],...
        'GridAlpha', 0.25,...
        'FontWeight','bold',...
        'FontSize', 22, ...
        'YLim', [0, route.model.max_airspeed + 15], ...
        'XLim', [0, t(end)] ...
    );
    box(axes1,'off');
    hold(axes1,'on');
    xlabel('Time (s)');
    ylabel('Airspeed (m/s)');
    
    % plot speed
    plot(t, speed, 'k-', 'LineWidth', 4);
    
        % plot limits
    r = refline(0, route.model.max_airspeed);
    r.LineWidth = 3;
    r.Color = 'r';
    r.LineStyle = '--';
    r = refline(0, route.model.min_airspeed);
    r.LineWidth = 3;
    r.Color = 'r';
    r.LineStyle = '--';
end