function [curv_poly_coeffs, Sf, flag] = init_curv_poly(degree, constraints)
%% Returns the polynomial coeffs and Sf for the curvature polynomial of degree 'degree',
%% subject to end-point constraints at Sf

    %% Constants
    Sf_min = 20;
    Sf_max = 1000;
    err_tol = 1e-4;
    num_samples = 100;
    Sf_vals = Sf_min:50:Sf_max;
    
    idx = 1;
    flag = false;
    while true
        Sf = Sf_vals(idx);
        
        %% Set up LHS
        C = zeros(3, degree);
        for i = degree:-1:1
            C(1, degree-i+1) = Sf ^ i;
            C(2, degree-i+1) = Sf ^ (i+1) / (i+1);
            C(3, degree-i+1) = i * Sf ^ (i-1);
        end

        %% Set up RHS
        d = [constraints.curv_ep; constraints.heading_ep; constraints.curv_deriv_ep];

        %% Set up box constraints
        lb = -5 * ones(degree, 1);
        lb(1) = -0.001;
        ub = 5 * ones(degree, 1);
        ub(1) = 0.001;

        %% Set up inequality constraints
        samples = transpose(linspace(0, Sf, num_samples));

        % Curvature constraint
        A_curv = [];
        for i = degree:-1:1
            A_curv = [A_curv [samples.^i; -samples.^i]];
        end
        b_curv = zeros(size(A_curv, 1), 1) + constraints.curv_max;

        % Curvature rate constraint
        A_deriv = [];
        for i = degree:-1:1
            A_deriv = [A_deriv [i * samples.^(i-1); -i * samples.^(i-1)]]; 
        end
        A_deriv = A_deriv / (samples(2) - samples(1));
        b_deriv = zeros(size(A_deriv, 1), 1) + constraints.curv_rate;

        A = [A_curv; A_deriv];
        b = [b_curv; b_deriv];

        options = optimset('Display', 'iter', 'MaxIter', 500, 'Algorithm', 'active-set');
        [curv_poly_coeffs, resnorm,residual,exitflag,output,lambda] = lsqlin(C, d, A, b, [], [], lb, ub, [], options);
        
        %% Check if we've found a satisfactory solution
        if max(abs(residual)) < err_tol
            flag = true;
            break;
        else
            idx = idx + 1;
        end
    end
end