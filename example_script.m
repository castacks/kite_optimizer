clear; clc; close all

%% Initialize model and parameters
[model, consts, allowed_err] = init_optimizer();

%% Define wind speed (assumed to be along x-axis)
wind_speed = 10;        % m/s

%% Get waypoints
waypoints = get_mission();

%% Get route
[route, flag] = get_complete_trajectory(waypoints, model, wind_speed, allowed_err, consts);

%% Display
display_path(route);
display_speed(route);
display_roll(route);
display_accel_jerk(route);


