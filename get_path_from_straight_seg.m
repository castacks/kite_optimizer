function [path_X, path_Y, path_heading, path_speed, path_bank, path_curv, path_times, path_deriv, path_roll_rate] = ...
    get_path_from_straight_seg(seg, wind_speed, samples, consts)
    
    ratio = samples / seg.s_air;
    S_ground = seg.s_ground * ratio;
    path_X = cos(seg.heading_g) * S_ground + seg.X0(1);
    path_Y = sin(seg.heading_g) * S_ground + seg.X0(2);
    
%     % find v and psi profile
%     V_new = sqrt(seg.v_in^2 + 2*seg.a_air*samples);
%     psi_new = zeros(size(V_new));
%     for i = 1:length(psi_new)
%         psi_new = get_heading_from_deriv(theta_des, wind_speed, V_new(i));
%     end
    
    % set the rest to defaults for now
    path_heading = transpose(zeros(size(path_X)));
    path_speed = zeros(size(path_X));
    path_bank = zeros(size(path_X));
    path_curv = zeros(size(path_X));
    path_times = zeros(size(path_X));
    path_deriv = repmat([cos(seg.heading_g) sin(seg.heading_g)], length(path_X), 1);
    path_roll_rate = zeros(size(path_X));
end