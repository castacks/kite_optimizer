function [C1, tf1, C2, tf2, amid] = lookup_accel_spline(accels, positive, count)
    if positive == 1
        idx = length(accels.accel_list) - (count-1);
    else
        idx = count;
    end
    
    C1 = accels.accel_list{idx}.c1;
    C2 = accels.accel_list{idx}.c2;
    tf1 = accels.accel_list{idx}.tf1;
    tf2 = accels.accel_list{idx}.tf2;
    amid = accels.accel_list{idx}.amid;
end