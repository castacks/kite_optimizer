function [flag] = tunnel_check(curv, tunnel1, tunnel2)
    flag = all(curv_in_tunnel(tunnel1, curv) + curv_in_tunnel(tunnel2, curv));
end