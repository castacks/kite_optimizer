function [ret, dist] = point_in_corridor(P, P1, P2, margin)
    ret = true;
    
    v = P2 - P1;
    w = P - P1;
    c1 = dot(w, v); 
    c2 = dot(v, v);
    if c1 <= 0
        dist = norm(P - P1);
        ret = false;
    elseif c2 <= c1
        dist = norm(P - P2);
        ret = false;
    else
        b = c1 / c2;
        Pb = P1 + b*v;
        dist = norm(P - Pb);
        if  dist > margin
            ret = false;
        end
        dist = dist - margin;
    end
    
end