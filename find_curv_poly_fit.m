function [X0, X_end, matched] = find_curv_poly_fit(curve_endP, waypts, dist_tol)
    max_iter = 1000;

    matched = false;
    low = 0;
    high = 1;
    t = 0.5;
    num_iter = 0;
    
    while t < 1 && num_iter < max_iter
        t = (low + high) / 2;
        X0 = waypts(1, 1:2) + t * (waypts(2, 1:2) - waypts(1, 1:2));
        X_end = X0 + curve_endP;

        % check if we've converged
        dist = point_dist_from_line(X_end, waypts(2, 1:2), waypts(3, 1:2));
        if dist < dist_tol
            if norm(X_end - waypts(2, 1:2)) > norm(waypts(3, 1:2) - waypts(2, 1:2))
                matched = false;
            else
                matched = true;
            end
            return;
        end

        % Shift start point otherwise
        if point_bw_segments(X_end, [waypts(1,1) waypts(1,2)], [waypts(2,1) waypts(2,2)], ...
                [waypts(3,1) waypts(3,2)])
            low = t;
        else
            high = t;
        end
        
        num_iter = num_iter + 1;
    end
end