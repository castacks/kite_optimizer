function [route, flag] = construct_trajectory(route, wind_speed, consts, waypts, model)
    tvals = 0:consts.TIME_STEP:route.total_t;
    svals = ppval(route.s_prof, tvals);
    
    curr_sf = 0; curr_idx = 1;
    X = []; Y = []; dydx = []; curv_heading = [];
    assumed_roll = []; assumed_roll_rate = [];
    breaks = [1];
    for i = 1:length(route.augmented_turns)
        turn = route.augmented_turns{i};
        
        idx = find(svals > (curr_sf + turn.Sf_total), 1);
        if i == length(route.augmented_turns)
            % we've reached the end
            idx = length(svals) + 1;
        end
        samples = svals(curr_idx:idx-1) - curr_sf;
        route.augmented_turns{i}.time_until = tvals(idx-1);
        if turn.straight
            [path_X, path_Y, heading, ~, path_roll, ~, ~, path_dydx, path_roll_rate] = ...
                get_path_from_straight_seg(turn, wind_speed, samples, consts);
        else    
            [path_X, path_Y, heading, ~, path_roll, ~, ~, path_dydx, path_roll_rate] = ...
                get_path_from_piecewise_poly(turn, wind_speed, samples, consts);
        end
        X = [X path_X]; Y = [Y path_Y]; dydx = [dydx; path_dydx];
        curv_heading = [curv_heading; heading];
        assumed_roll = [assumed_roll path_roll];
        assumed_roll_rate = [assumed_roll_rate path_roll_rate];
        breaks = [breaks; idx-1];
        
        route.augmented_turns{i}.idx_until = length(X);
        curr_idx = idx;
        curr_sf = curr_sf + turn.Sf_total;
    end
    % Get actual heading and groundspeed
    actual_heading = wrapTo2Pi(atan2(dydx(:, 2), dydx(:, 1)));
    
    v_air = transpose(ppval(route.vel_prof, tvals));
    D = v_air.^2 - (wind_speed^2) * (sin(actual_heading)).^2;
    D(D < 0) = 0; % ensure groundspeed is not imaginary
    v_ground = wind_speed * cos(actual_heading) + sqrt(D);
    
    % Re-estimate time profile
    S_g = zeros(size(v_ground));
    for i = 2:length(S_g)
        S_g(i) = S_g(i-1) + norm([X(i)-X(i-1), ...
            Y(i)-Y(i-1)]);
    end
    new_times = zeros(size(S_g));
    vg_inv = 1 ./ v_ground;
    for i = 2:length(new_times)
        new_times(i) = trapz(S_g(1:i), vg_inv(1:i));
    end
    tvals = new_times;
    tvals(end) = tvals(end-1) + consts.TIME_STEP;
    
    % Get ground-frame derivatives
    X_dot = first_deriv(X, tvals);
    Y_dot = first_deriv(Y, tvals);
    X_ddot = second_deriv(X, tvals);
    Y_ddot = second_deriv(Y, tvals);
    X_dddot = second_deriv(X_dot, tvals);
    Y_dddot = second_deriv(Y_dot, tvals);
    th_dot = get_heading_dot(actual_heading, tvals);
    
    % Get airspeed's accel and jerk
    a_air = first_deriv(v_air, tvals);
    j_air = second_deriv(v_air, tvals);
    
    % Get groundspeed's accel and jerk
    a_ground = (v_air.*a_air - wind_speed*v_ground.*sin(actual_heading).*th_dot) ./ ...
            (v_ground - wind_speed*cos(actual_heading));
    j_ground = first_deriv(a_ground, consts.TIME_STEP);
    
    % Get airframe heading
    heading_a = wrapTo2Pi(atan2(v_ground.*dydx(:, 2), v_ground.*dydx(:, 1)-wind_speed));
    heading_dot = get_heading_dot(heading_a, tvals);
    
    % Get roll and roll-rate
    actual_roll = atan2(v_air.*heading_dot, consts.g);
    actual_roll_rate = first_deriv(actual_roll, tvals);
    actual_roll_ddt = first_deriv(actual_roll_rate, tvals);
    
    % get curv and curv rate
    actual_curv = heading_dot ./ v_air;
    actual_curv_rate = first_deriv(actual_curv, tvals);
    
    trajectory.X = X;
    trajectory.Y = Y;
    trajectory.X_dot = X_dot;
    trajectory.Y_dot = Y_dot;
    trajectory.X_ddot = X_ddot;
    trajectory.Y_ddot = Y_ddot;
    trajectory.X_dddot = X_dddot;
    trajectory.Y_dddot = Y_dddot;
    trajectory.airspeed = v_air;
    trajectory.t_vals = tvals';
    trajectory.groundspeed = v_ground;
    trajectory.accel = a_ground;
    trajectory.jerk = j_ground;
    trajectory.actual_heading = actual_heading;
    trajectory.actual_heading_dot = th_dot;
    trajectory.curv_heading = curv_heading;
    trajectory.breaks = breaks;
    trajectory.air_heading = heading_a;
    trajectory.assumed_roll = assumed_roll;
    trajectory.assumed_roll_rate = assumed_roll_rate;
    trajectory.actual_roll = actual_roll;
    trajectory.actual_roll_rate = actual_roll_rate;
    trajectory.actual_roll_ddt = actual_roll_ddt;
    trajectory.a_air = a_air;
    trajectory.j_air = j_air;
    trajectory.S_ground = S_g(end);
    trajectory.actual_curv = actual_curv;
    trajectory.actual_curv_rate = actual_curv_rate;
    
    route.trajectory = trajectory;
    
    % get z profile
    [route, flag] = get_z_profile(route, waypts, consts, model);
    if ~flag
        fprintf('Failed to compute z-profile');
    end
end