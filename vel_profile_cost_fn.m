function [cost] = vel_profile_cost_fn(X, seg_s, seg_v, seg_a, seg_j, seg_maxV, bv, consts, use_time)
    
    if nargin < 9
        use_time = false;
    end
%     [tf, ~, ~] = get_vels_from_constraints(X, seg_s, seg_v, seg_a, seg_j, bv, consts);
    
%     cost = sum(tf);

%       cost = -sum(X);

    [tf, feasible] = get_vels_from_bv(X, seg_s, seg_v, seg_maxV, bv, consts);
    
    if use_time
        cost = sum(tf);
    else
        cost = -sum(X);
    end
end