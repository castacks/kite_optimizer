clear; clc; 
%% Defines
consts.CURV_POLY_DEG = 2;
consts.SPEED_POLY_DEG = 1;
consts.NUM_SAMPLES = 100;
consts.TIME_WEIGHT = 1.0;
consts.CURV_SMOOTH_WEIGHT = 0.0;

%% Allowed errors
allowed_err.curv = 0.0001;
allowed_err.heading = deg2rad(5);
allowed_err.speed = 5;
allowed_err.pos = 1;

%% Inputs
waypoints = csvread('route.csv'); % assume x-axis is aligned with wind
wind_speed = 10;  % m/s

%% Model
model.g = 9.81;
model.max_airspeed = 30;    % m/s
model.max_roll = deg2rad(30);
model.max_roll_rate = deg2rad(15);
model.max_a = 0.75 * model.g;
model.curv_max = model.g * tan(model.max_roll) / (model.max_airspeed^2);
% model.curv_rate_max = 1.9e-04;
model.curv_rate_max = model.g * model.max_roll_rate / (model.max_airspeed^3);

%% Parse route
angles = normr(diff(waypoints(:, 1:2)));
angles = atan2(angles(:, 2), angles(:, 1));
angles = wrapToPi(angles);

%% Get curvature polynomial
heading1 = angles(1);
heading2 = angles(2);
[curv_poly_params, Sf] = get_curv_poly(heading1, heading2, consts, model, allowed_err, wind_speed, waypoints);

 %% Display path
 disp_path([(curv_poly_params(1:consts.CURV_POLY_DEG))'; Sf], wind_speed, waypoints, heading1, heading2, consts, model);
 
 %% Check for constraint violations
 violations = curv_constraints_checker([(curv_poly_params(1:consts.CURV_POLY_DEG))'; Sf], ...
     wind_speed, waypoints, heading1, heading2, consts, model, allowed_err);