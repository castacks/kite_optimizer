function [flag] = point_in_triangle(P, vert)
% vert = [x1 x2 x3; y1 y2 y3]    

    % Get barycentric coordinates
    R = [vert; 1 1 1];
    b = R \ [P; 1];
    
    flag = (length(find(b > 0)) == 3) &&  (length(find(b < 1)) == 3);
end