function [X_init] = velopt_init_fn(seg_v, seg_s, seg_maxV, bv, a)

    X_init = seg_v(1:end-1) - 0.5;
    
    % First, make sure the velocities adjacent to the boundary values are
    % feasible
    v1 = bv.init(1);
    v2 = X_init(1);
    if abs(v1^2 - v2^2) / (2*a) > seg_s(1)
        % shift v2 to be feasible
        X_init(1) = sqrt(v1^2 + sign(v2-v1) * 2 * a * seg_s(1));
    end
    % Now adjust the value to the right of v1
    v1 = X_init(1);
    v2 = X_init(2);
    if abs(v1^2 - v2^2) / (2*a) > seg_s(2)
        % shift v2 to be feasible
        X_init(2) = sqrt(v1^2 + sign(v2-v1) * 2 * a * seg_s(2));
    end
    
    v1 = X_init(end);
    v2 = bv.end(1);
    if abs(v1^2 - v2^2) / (2*a) > seg_s(end)
        % shift v1 to be feasible
        X_init(end) = sqrt(v2^2 - sign(v2-v1) * 2 * a * seg_s(end));
    end
    % Now adjust the value to the left of v1
    v1 = X_init(end-1);
    v2 = X_init(end);
    if abs(v1^2 - v2^2) / (2*a) > seg_s(end-1)
        % shift v1 to be feasible
        X_init(end-1) = sqrt(v2^2 - sign(v2-v1) * 2 * a * seg_s(end-1));
    end
    
    visited = zeros(size(X_init));
    visited(1) = 1;
    visited(end) = 1;
    
    while ~all(visited)
        % find smallest unvisited velocity
        [X_sorted, inds] = sort(X_init);
        idx = 1;
        while visited(inds(idx))
            idx = idx + 1;
            if idx > length(inds)
                break;
            end
        end
        
        % Make the right and left neighbors of this velocity feasible
        idx = inds(idx);
        % process left neighbor
        if visited(idx-1) ~= 1
            v1 = X_init(idx-1);
            v2 = X_init(idx);
            if abs(v1^2 - v2^2) / (2*a) > seg_s(idx)
                % shift v1 to be feasible
                X_init(idx-1) = sqrt(v2^2 - sign(v2-v1) * 2 * a * seg_s(idx));
            end
        end
        
        % process right neighbor
        if visited(idx+1) ~= 1
            v1 = X_init(idx);
            v2 = X_init(idx+1);
            if abs(v1^2 - v2^2) / (2*a) > seg_s(idx+1)
                % shift v2 to be feasible
                X_init(idx+1) = sqrt(v1^2 + sign(v2-v1) * 2 * a * seg_s(idx+1));
            end
        end
        
        visited(idx) = 1;
    end
end