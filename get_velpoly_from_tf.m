function [vel_poly_coeffs, flag, error] = get_velpoly_from_tf(degree, constraints, tf, init)
    if nargin < 4
        init = false;
    end

    err_tol = 1e-4;
    num_samples = 500;
    flag = false;
    
    %% Set up LHS
    % boundary values
    C_bv = [];
    if ~isempty(constraints.bv)
        C_bv = zeros(2, degree+1);
        for i = degree:-1:0
            C_bv(1, degree-i+1) = 0 ^ i;
            C_bv(2, degree-i+1) = tf ^ i;
        end
    end

    % boundary derivative values
    C_bv_deriv = [];
    if ~isempty(constraints.bv_deriv)
        C_bv_deriv = zeros(2, degree+1);
        for i = degree:-1:1
            C_bv_deriv(1, degree-i+1) = i * 0 ^ (i-1);
            C_bv_deriv(2, degree-i+1) = i * tf ^ (i-1);
        end
    end
    
    % boundary double-derivative vals
%     C_bv_dderiv = zeros(1, degree+1);
    C_bv_dderiv = [];
%     if ~isempty(constraints.bv_dderiv)
%         C_bv_dderiv = zeros(1, degree+1);
%         for i = degree:-1:2
%             C_bv_dderiv(1, degree-i+1) = i * (i-1) * tf ^ (i-2);
%         end
%     end
    
    % boundary integral val
    C_bv_integral = [];
    if ~isempty(constraints.bv_integral)
        C_bv_integral = zeros(1, degree+1);
        for i = degree:-1:0
            C_bv_integral(1, degree-i+1) = tf^(i+1) / (i+1);
        end
    end

    C = [C_bv; C_bv_deriv; C_bv_dderiv; C_bv_integral];

    %% Set up RHS
    d = [constraints.bv; constraints.bv_deriv; constraints.bv_dderiv; constraints.bv_integral];

    if init
        %% Set up box constraints
        lb = [];
        ub = [];
%         lb = -5 * ones(degree+1, 1);
%         lb(1) = -0.001;
%         ub = 5 * ones(degree+1, 1);
%         ub(1) = 0.001;

        %% Set up inequality constraints
        samples = transpose(linspace(0, tf, num_samples));

        % Speed constraint
        A_speed = [];
        for i = degree:-1:0
            A_speed = [A_speed [samples.^i; -samples.^i]];
        end
        b_speed = zeros(size(A_speed, 1), 1);
        b_speed(1:size(A_speed, 1)/2) = constraints.speed_max;

        % rate constraint
        A_deriv = [];
        for i = degree:-1:1
            A_deriv = [A_deriv [i * samples.^(i-1); -i * samples.^(i-1)]]; 
        end
        A_deriv = [A_deriv zeros(2*length(samples), 1)];
%         A_deriv = A_deriv / (samples(2) - samples(1));
        b_deriv = zeros(size(A_deriv, 1), 1) + constraints.accel;
        
        % rate-rate constraint
        A_dderiv = [];
        for i = degree:-1:2
            A_dderiv = [A_dderiv [i * (i-1) * samples.^(i-2); -i * (i-1) * samples.^(i-2)]]; 
        end
        A_dderiv = [A_dderiv zeros(2*length(samples), 2)];
%         A_dderiv = A_dderiv / (samples(2) - samples(1));
        b_dderiv = zeros(size(A_dderiv, 1), 1) + constraints.jerk;        

%         A = [A_speed; A_deriv; A_dderiv];
%         b = [b_speed; b_deriv; b_dderiv];
        A = [A_speed; A_dderiv; A_deriv];
        b = [b_speed; b_dderiv; b_deriv];

        options = optimset('MaxIter', 200, 'Algorithm', 'active-set', 'display', 'off');
        [vel_poly_coeffs, resnorm,residual,exitflag,output,lambda] = lsqlin(C, d, A, b, [], [], lb, ub, [], options);
        vel_poly_coeffs = vel_poly_coeffs';
        %% Check if we've found a satisfactory solution
        error = max(abs(residual));
        if  error < err_tol && output.constrviolation < eps
            flag = true;
        end
    end
    
%     curv_poly_coeffs = (C \ d)';
end