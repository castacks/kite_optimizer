function [path_X, path_Y, path_heading, path_speed, path_bank, path_curv, path_times, path_deriv, path_roll_rate] = ...
    get_path_structured(X, wind_speed, waypoints, heading1, heading2, consts, model, constraints1, constraints3, samples1, samples2, samples3)
    
    Sf1 = X(1); Sf2 = X(2); Sf3 = X(3); k_trans = X(4);

    if nargin < 10
        samples1 = transpose(linspace(0, Sf1, consts.NUM_SEGMENT_SAMPLES));
        samples2 = transpose(linspace(0, Sf2, consts.NUM_SEGMENT_SAMPLES));
        samples3 = transpose(linspace(0, Sf3, consts.NUM_SEGMENT_SAMPLES));
    end

    constraints1.bv = [0; k_trans];
    C1 = get_poly_from_sf(consts.CURV_POLY_DEG, constraints1, Sf1);
    C2 = k_trans;
    constraints3.bv = [k_trans; 0];
    C3 = get_poly_from_sf(consts.CURV_POLY_DEG, constraints3, Sf3);
    X0 = [0 0];
    speed = model.max_airspeed;
    
    [path_X1, path_Y1, path_heading1, path_speed1, path_bank1, path_curv1, path_times1, path_deriv1, path_roll_rate1] = ...
        get_path_from_poly(C1, speed, Sf1, heading1, X0, wind_speed, consts, model, 0, samples1);
    
    [path_X2, path_Y2, path_heading2, path_speed2, path_bank2, path_curv2, path_times2, path_deriv2, path_roll_rate2] = ...
        get_path_from_poly(C2, speed, Sf2, path_heading1(end), [path_X1(end) path_Y1(end)], wind_speed, consts, model, path_times1(end), samples2);
    
    [path_X3, path_Y3, path_heading3, path_speed3, path_bank3, path_curv3, path_times3, path_deriv3, path_roll_rate3] = ...
        get_path_from_poly(C3, speed, Sf3, path_heading2(end), [path_X2(end) path_Y2(end)], wind_speed, consts, model, path_times2(end), samples3);
    
    %% Put everything together
    path_X = [path_X1; path_X2; path_X3];
    path_Y = [path_Y1; path_Y2; path_Y3];
    path_heading = [path_heading1; path_heading2; path_heading3];
    path_speed = [path_speed1; path_speed2; path_speed3];
    path_bank = [path_bank1; path_bank2; path_bank3];
    path_curv = [path_curv1; path_curv2; path_curv3];
    path_times = [path_times1; path_times2; path_times3];
    path_deriv = [path_deriv1; path_deriv2; path_deriv3];
    path_roll_rate = [path_roll_rate1; path_roll_rate2; path_roll_rate3];
end