# README #

This package contains the MATLAB implementation for kITE, a fast trajectory optimizer that generates time-optimal, smooth and dynamically feasbile trajectories while explicitly accounting for moving reference frames such as wind. 

### Running the code ###
Please run example script:
```
#!matlab

example_script
```

### Changing parameters ###
The dynamic limits of the system are defined in init_optimizer.m. Limits corresponding to speed, acceleration, jerk, bank and bank rate can be set here. The algorithm makes use of lookup tables (in lut/) to speed up the search for optimal acceleration and curvature profiles. If the dynamic limits need to be changed, please re-generate the lookup tables by changing the appropriate limits in the following scripts and executing them-
```
#!matlab

dump_accel_polys
dump_curv_polys
```

### Defining missions ###
Missions can be defined as a list of waypoints, following the format shown in ```get_mission()```. Please follow the directions in the comments for that function.

### Questions? ###

* Vishal Dugar (vdugar@cmu.edu)