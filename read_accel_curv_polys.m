function [accels, curvs] = read_accel_curv_polys(accel_fn, curv_fn)
    accel_mat = dlmread(accel_fn);
    curv_mat = dlmread(curv_fn);
    
    % Read accel polys
    accels.num_accel = accel_mat(1, 1);
    accels.accel_list = cell(accels.num_accel, 1);
    for i = 2:accels.num_accel+1
        poly.amid = accel_mat(i, 1);
        poly.tf1 = accel_mat(i, 2);
        poly.tf2 = accel_mat(i, 3);
        poly.c1 = [accel_mat(i, 4), accel_mat(i, 5), accel_mat(i, 6), accel_mat(i, 7)];
        poly.c2 = [accel_mat(i, 8), accel_mat(i, 9), accel_mat(i, 10), accel_mat(i, 11)];
        
        accels.accel_list{i-1} = poly;
    end
    
    % Read curv polys
    curvs.num_vels = curv_mat(1, 1);
    curvs.num_curv = curv_mat(1, 2);
    curvs.min_vel = curv_mat(1, 3);
    curvs.max_vel = curv_mat(1, 4);
    curvs.vel_step = curv_mat(1, 5);
    curvs.curv_list = cell(curvs.num_vels*curvs.num_curv, 1);
    for i = 2:curvs.num_vels*curvs.num_curv+1
        poly.vel = curv_mat(i, 1);
        poly.ktrans = curv_mat(i, 2);
        poly.Sf1 = curv_mat(i, 3);
        poly.Sf2 = curv_mat(i, 4);
        poly.c1 = [curv_mat(i, 5), curv_mat(i, 6), curv_mat(i, 7), curv_mat(i, 8), curv_mat(i, 9)];
        poly.c2 = [curv_mat(i, 10), curv_mat(i, 11), curv_mat(i, 12), curv_mat(i, 13), curv_mat(i, 14)];
        
        curvs.curv_list{i-1} = poly;
    end
end