function [X_end] = get_turn_end_point(turn, air_heading0, wind)
    airspeed = turn.limits.max_airspeed;
    time_fn = @(x) (polyval(airspeed, x)).^-1;
    
    % first segment
    psi1 = polyint(turn.C1, air_heading0);
    fun_X = @(x) cos(wrapTo2Pi(polyval(psi1, x)));
    fun_Y = @(x) sin(wrapTo2Pi(polyval(psi1, x)));
    time1 = integral(time_fn, 0, turn.Sf1);
    x = integral(fun_X, 0, turn.Sf1);
    y = integral(fun_Y, 0, turn.Sf1);
    
    % second segment
    psi2 = polyint(turn.C2, polyval(psi1, turn.Sf1));
    fun_X = @(x) cos(wrapTo2Pi(polyval(psi2, x)));
    fun_Y = @(x) sin(wrapTo2Pi(polyval(psi2, x)));
    time2 = integral(time_fn, 0, turn.Sf2);
    x = x + integral(fun_X, 0, turn.Sf2);
    y = y + integral(fun_Y, 0, turn.Sf2);
    
    % third segment
    psi3 = polyint(turn.C3, polyval(psi2, turn.Sf2));
    fun_X = @(x) cos(wrapTo2Pi(polyval(psi3, x)));
    fun_Y = @(x) sin(wrapTo2Pi(polyval(psi3, x)));
    time3 = integral(time_fn, 0, turn.Sf3);
    x = x + integral(fun_X, 0, turn.Sf3);
    y = y + integral(fun_Y, 0, turn.Sf3);
    
    X_end = [x + wind*(time1+time2+time3), y];
end