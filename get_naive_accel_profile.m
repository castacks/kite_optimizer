function [prof] = get_naive_accel_profile(vel_init, vel_final, Sf, limits, consts, S_0)
    %% Check for the degenerate case
    if abs(vel_init - vel_final) < 0.1
        prof.valid = true;
        prof.empty = true;
        prof.vel_init = vel_init;
        prof.vel_final = vel_init;
        prof.Sf_total = Sf;
        prof.Sf1 = 0; prof.Sf2 = 0; prof.Sf3 = 0; prof.Sf4 = Sf;
        prof.vel_C1 = [];
        prof.vel_C2 = [];
        prof.vel_C3 = [];
        prof.vel_C4 = [zeros(1,consts.ACCEL_POLY_DEG+2-1) vel_final];
        prof.Sf_C4 = polyint(prof.vel_C4, S_0);
        prof.tf1 = 0;
        prof.tf2 = 0;
        prof.tf3 = 0;
        prof.tf4 = Sf / vel_init;
        prof.tf_total = prof.tf4;
        
        return;
    end
    
    %% Initialize
    if vel_final > vel_init
        a_high = limits.max_accel;
        a_low = 0;
        avals = linspace(a_high, a_low, 20);
        positive = 1;
    else
        a_low = -limits.max_accel;
        a_high = 0;
        avals = linspace(a_low, a_high, 20);
        positive = 0;
    end
    
    %% Search to find the best accel for the job
    for i = 1:consts.accels.num_accel/2
%     for a_trans = avals
%         constraints1.bv = [0; a_trans];
%         constraints1.bv_deriv = [0; 0];
%         constraints1.jerk = limits.max_jerk;
%         constraints1.accel = limits.max_accel;
%         
%         constraints3.bv = [a_trans; 0];
%         constraints3.bv_deriv = [0; 0];
%         constraints3.jerk = limits.max_jerk;
%         constraints3.accel = limits.max_accel;
%         
%         [C1, tf1, flag1] = init_accel_poly(consts.ACCEL_POLY_DEG, constraints1);
%         [C3, tf3, flag2] = init_accel_poly(consts.ACCEL_POLY_DEG, constraints3);

        % Look-up accel polys
        [C1, tf1, C3, tf3, a_trans] = lookup_accel_spline(consts.accels, positive, i);
        
%         if ~flag1 || ~flag2
%             continue;
%         end
        
        [tf2, Sf_used, vel_C1, vel_C2, vel_C3, flag] = ...
            check_valid_accel(tf1, C1, tf3, C3, a_trans, vel_init, vel_final, Sf, consts);
        
        if flag
            prof.valid = true;
            prof.empty = false;
            prof.vel_init = vel_init;
            prof.vel_final = vel_final;
            prof.vel_C1 = vel_C1;
            prof.vel_C2 = vel_C2;
            prof.vel_C3 = vel_C3;
            prof.vel_C4 = [zeros(1,consts.ACCEL_POLY_DEG+2-1) vel_final];
            prof.Sf_total = Sf;
            
            prof.tf1 = tf1;
            prof.tf2 = tf2;
            prof.tf3 = tf3;
            if vel_final > vel_init
                prof.tf4 = (Sf - Sf_used) / vel_final;
            else
                % Shift to schedule deceleration later along S when vel_final = 0
                prof.tf4 = prof.tf3; prof.vel_C4 = prof.vel_C3;
                prof.tf3 = prof.tf2; prof.vel_C3 = prof.vel_C2;
                prof.tf2 = prof.tf1; prof.vel_C2 = prof.vel_C1;
                prof.tf1 = (Sf - Sf_used) / vel_init;
                prof.vel_C1 = [zeros(1,consts.ACCEL_POLY_DEG+2-1) vel_init];
            end
            
            prof.Sf_C1 = polyint(prof.vel_C1, S_0);
            prof.Sf_C2 = polyint(prof.vel_C2, polyval(prof.Sf_C1, prof.tf1));
            prof.Sf_C3 = polyint(prof.vel_C3, polyval(prof.Sf_C2, prof.tf2));
            prof.Sf_C4 = polyint(prof.vel_C4, polyval(prof.Sf_C3, prof.tf3));
            prof.Sf1 = polyval(prof.Sf_C1, prof.tf1);
            prof.Sf2 = polyval(prof.Sf_C2, prof.tf2);
            prof.Sf3 = polyval(prof.Sf_C3, prof.tf3);
            prof.Sf4 = polyval(prof.Sf_C4, prof.tf4);
            return;
        end
    end
    
    prof.valid = false;
end