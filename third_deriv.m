function [d3_dx3] = third_deriv(X, h)
    d3_dx3 = ( (-1/2)*X(1:end-4)  + X(2:end-3) - X(4:end-1) + (1/2)*X(5:end) ) / (h^3);
end