function [path_X, path_Y, path_heading, path_speed, path_bank, path_curv, path_times, path_deriv, path_roll_rate] = ...
    get_path_from_poly(curv_poly, speed_poly, Sf, heading0, X0, wind_speed, consts, model, start_time, samples)
    
    %% get heading function
    psi = polyint(curv_poly, heading0);
    fun_X = @(x) cos(wrapTo2Pi(polyval(psi, x)));
    fun_Y = @(x) sin(wrapTo2Pi(polyval(psi, x)));
    
    %% Evaluate sample times and path
    time_fn = @(x) (polyval(speed_poly, x)).^-1;
    
    path_times = zeros(size(samples));
    path_X = zeros(size(samples));
    path_Y = zeros(size(samples));
    path_heading = zeros(size(samples));
    path_speed = zeros(size(samples));
    path_bank = zeros(size(samples));
    path_curv = zeros(size(samples));
    path_deriv = zeros(length(samples), 2);
    
    for i = 1:length(samples)
        vel = polyval(speed_poly, samples(i));
        path_times(i) = integral(time_fn, 0, samples(i));
        path_heading(i) = polyval(psi, samples(i));
        path_speed(i) = vel;
        path_curv(i) = polyval(curv_poly, samples(i));
        path_X(i) = integral(fun_X, 0, samples(i)) + path_times(i) * wind_speed;
        path_Y(i) = integral(fun_Y, 0, samples(i));
        path_bank(i) = wrapToPi(atan2(path_speed(i)*path_speed(i)*path_curv(i), model.g));
        V_x = vel * cos(path_heading(i)) + wind_speed;
        V_y = vel * sin(path_heading(i));
        V = sqrt(V_x^2 + V_y^2);
        path_deriv(i, 1) = V_x / V;
        path_deriv(i, 2) = V_y / V;
%         path_deriv(i) = vel * sin(path_heading(i)) / (vel * cos(path_heading(i)) + wind_speed);
    end
    path_X = path_X + X0(1);
    path_Y = path_Y + X0(2);
%     path_deriv = diff(path_Y) ./ diff(path_X);
    path_roll_rate = diff(path_bank) ./ diff(path_times);
    path_times = path_times + start_time;
    
    path_heading = wrapTo2Pi(path_heading);
end