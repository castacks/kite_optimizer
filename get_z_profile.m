function [route, flag] = get_z_profile(route, waypts, consts, model)
% Fits the z-profile to route
    
    flag = true;

    init_z = waypts(1, 3);
    Z = zeros(length(route.trajectory.X), 1);
    
    prev_z = init_z;
    prev_time = 0;
    prev_idx = 0;
    for i = 2:size(waypts, 1)
        zdest = waypts(i, 3);
        seg_idx = route.waypts_map(i);
        avail_time = route.augmented_turns{seg_idx}.time_until - prev_time;
        curr_idx = route.augmented_turns{seg_idx}.idx_until;
        num_samples = curr_idx - prev_idx;
        roc = (zdest-prev_z) / avail_time;
        
        if abs(roc) > model.max_vel_z;
            % fail
            flag = false;
            return;
        end
        
        t = linspace(0, avail_time, num_samples);
        Z(prev_idx+1:curr_idx) = prev_z + roc * t;
        
        prev_time = route.augmented_turns{seg_idx}.time_until;
        prev_z = zdest;
        prev_idx = curr_idx;
    end
    
    Z = Z';
    route.trajectory.Z = Z;
    route.trajectory.Z_dot = first_deriv(Z, consts.TIME_STEP);
    route.trajectory.Z_ddot = second_deriv(Z, consts.TIME_STEP);
    route.trajectory.Z_dddot = third_deriv(Z, consts.TIME_STEP);
end