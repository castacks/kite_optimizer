function [limits] = get_turn_limits(waypts, model, ignore_speed)
    if nargin < 3
        ignore_speed = false;
    end

    limits.g = model.g;
    if ignore_speed
        limits.max_airspeed = floor(model.max_airspeed);
    else
        limits.max_airspeed = floor(max(waypts(2:3, 4)));
    end
    
    % make airspeed an even number, to match the curv lookup table
    if mod(limits.max_airspeed, 2) ~= 0
        limits.max_airspeed = limits.max_airspeed - 1;
    end
    
%     limits.max_airspeed = 15;
    limits.max_accel = model.max_accel;
    limits.max_jerk = model.max_jerk;
    limits.curv_max = model.g * tan(model.max_roll) / (limits.max_airspeed^2);
%     limits.curv_rate_max = model.g * model.max_roll_rate / (limits.max_airspeed^3);
    limits.curv_rate_max = model.g * model.max_roll_rate / (limits.max_airspeed^3) - 2 * limits.curv_max * limits.max_accel / (model.max_airspeed^2);
    limits.curv_rate_rate_max = model.g * model.max_roll_rate_rate / (limits.max_airspeed^4);
    limits.max_roll = model.max_roll;
    limits.max_roll_rate = model.max_roll_rate;
    limits.max_roll_rate_rate = model.max_roll_rate_rate;
end