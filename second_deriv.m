function [d2_dx2] = second_deriv(X, h)
    X = X(:);
    h = h(:);

    if length(h) == 1
        d2_dx2 = ( X(1:end-2) - 2*X(2:end-1) + X(3:end) ) / (h^2);
    else
        h = diff(h);
        h = h(1:end-1);
        d2_dx2 = ( X(1:end-2) - 2*X(2:end-1) + X(3:end) ) ./ (h.^2);
    end
    
    % set length equal to input
    if size(d2_dx2, 2) ~= 1
        d2_dx2 = d2_dx2';
    end
    d2_dx2 = [0; d2_dx2; 0];
end