function [d_dx] = first_deriv(X, h)
    X = X(:);
    h = h(:);
    
    if length(h) == 1
        d_dx = (-(1/2)*X(1:end-2) + (1/2)*X(3:end)) / h;
    else
        % assume correct length is passed
        h = diff(h);
        h = h(1:end-1);
        d_dx = (-(1/2)*X(1:end-2) + (1/2)*X(3:end)) ./ h;
    end
    
    % set length equal to input
    if size(d_dx, 2) ~= 1
        d_dx = d_dx';
    end
    d_dx = [0; d_dx; 0];
end